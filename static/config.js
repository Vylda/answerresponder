/*
   API_URL
    Cesta, kde jsou umístěny PHP skripty (pokud je máš tak, jak jsi stáhnul, tak ji neměň)
    The path where the PHP scripts are located (if you have them in the way you downloaded, don't change them)
  DEFAULT_LANGUAGE
    Pokud nebude nalezen překlad, automaticky se použije tento překlad (musí existovat)
    If no translation is found, this translation will be used automatically (it must exist)
  WEEK_STARTS_IN
    začátek týdne podle jazyka: 0 = neděle … 6 = sobota
    start of week based languahe: 0 = Sunday … 6 = Saturday
  INSTANCE_NAME
    jedinečné jméno hry; neměňte, pokud na jednom serveru běží pouze jedna instance hry
    unique name of game; don't change, if on your server running only one instance of game
*/
var gameConfig = {
  API_URL: './backend',
  DEFAULT_LANGUAGE: 'cs',
  WEEK_STARTS_IN: {
    cs: 1, // Monday, pondělí
    en: 0, // Sunday, neděle
  },
  INSTANCE_NAME: 'mainApp',
};

/*
Překlady
  Překlady se používají podle jejich jazykových kódů (cd, en, ale i en_US).
  Pokud nějaký překlad nechceš smaž je. Nezapomeň správně nastavit DEFAULT_LANGUAGE.
Translations
  Translations are used according to their language codes (cd, en, but also en_US).
  If you don't want a translation, delete it. Be sure to set DEFAULT_LANGUAGE correctly.
*/
var translations = {
  cs: {
    /*
    Název aplikace
    */
    name: 'Answer responder – demo',
    /* Nastavení aplikace */
    setTranslation: 'Nastavit jazyk aplikace',
    darkMode: 'Tmavý režim',
    deleteSettings: 'Smazat nastavení aplikace',
    personalDataDeleted: 'Všechna osobní data byla smazána.',
    /*
    Názvy tlačítek
    */
    button: {
      sendAnswer: 'Odeslat odpověď',
      loadGame: 'Nahrát hru',
      startFromZero: 'Začít od začátku',
    },
    /*
    Formulář
    */
    form: {
      /*
      Políčko pro e-mail
      */
      email: {
        label: '* Tvoje e-mailová adresa',
        placeholder: 'jmeno.prijmeni@domena.tld',
      },
      /*
      Políčko pro přezdívku
      */
      nick: {
        label: '* Tvoje přezdívka',
        placeholder: 'přezdívka (třeba na geocaching.com)',
      },
      /*
      Políčko pro odpověď
      */
      answer: {
        label: '* Tvoje odpověď',
        placeholder: 'odpověď',
      },
    },
    /*
    Úkoly hry
    */
    task: {
      /*
      Pouze první úkol, ostatní úkoly dostanou texty z API
      */
      first: {
        title: 'Vítej, hráči!',
        hints: `Tohle je demoverze hry, kterou si můžeš stáhnout a následně (při zachování informací z patičky) použít pro svoji vlastní hru.
          Každý úkol (kromě prvního) se dozvíš z e-mailu, který se pošle do tvé e-mailové schránky, stejně jako instrukce po posledním správně zodpovězeném úkolu.

          Tato demoverze má celkem šest otázek, které se nabídnou ve třech úrovních. V posledních dvou úrovních bude otázka vybrána náhodně.

          První úkol (který se obvykle dozvíš jinou cestou) zní takto:
          Skupina Queen měla v původní sestavě čtyři členy. Napiš jméno právě jednoho z nich!

          Nejdřív zkus odpovědět naprosto špatně, pak zkus napsat špatně původní Freddieho jméno, třeba Farok.
          Správné odpovědi jsou:
          Freddie, Mercury, Freddie Mercury, Farrokh, Bulsara, Farrokh Bulsara
          Brian, May, Brian May, Brian Harold May
          John, Deacon, John Deacon\nRoger, Taylor, Roger Taylor, Roger Meddows Taylor.
          Vyzkoušej si některé ze správných odpovědí, zejména ty s plnými jmény mají svou speciální odpověď.
          Všechny jiné odpovědi než zde uvedené, jsou špatně.

          Prosím, napiš do políčka pro e-mail svoji e-mailovou adresu, do políčka pro přezdívku vlož svoji přezdívku (třeba tu z geocaching.com) a do políčka pro odpověď napiš svoji %level%. odpověď.
          Pak klikni na tlačítko Odeslat odpověď.
          Pokud máš hru rozehranou, zadej svůj e-mail a klikni na tlačítko Nahrát hru.

          Více na webovce určené pro tuto hru (viz patička).`,
      },
      /*
      Špatná odpověď
      */
      badAnswerHeader: "Špatná odpověď!",
    },
    /*
    Okno pro obnovení hry
    */
    recovery: {
      title: 'Obnovení hry',
      info: `Zdá se, že jsi už hru jednou hrál, ale nepodařilo se mi ji obnovit.\nProsím, zadej své heslo pro obnovení, které jsi obdržel v prvním e-mailu.
        Po zadání hesla můžeš obnovit hru a pokračovat, nebo smazat hru a začít od začátku.
        Pokud heslo neznáš, zavři tuto kartu a začni s jiným e-mailem nebo kontaktuj autora hry.`,
      label: '* Tvoje heslo pro obnovení',
      placeholder: 'heslo pro obnovení',
      errorEmptyPassword: 'Heslo pro obnovení musí být zadáno!',
      close: 'Zavřít',
      delete: 'Smazat hru',
      recover: 'Pokračovat ve hře',
      show: 'Ukázat',
      hide: 'Skrýt',
      showHidePassword: '{showHide} heslo',
    },
    /*
    Chybové hlášky
    */
    error: {
      noBackend:
        'Nepodařilo se připojit k serveru. Dnes to asi nepůjde. Prosím, napiš autorovi hry a popiš, co jsi dělal!',
      backend:
        'Dnes to asi nepůjde, nedaří se mi připojit. Prosím, popiš autorovi hry to, co děláš!',
      backendHeader: 'Ajajaj, něco se stalo, protože jsem zjistil problém…',
      recovery: {
        recover:
          'Hru jsem nemohl obnovit, protože se mi nepodařilo připojit k serveru.',
        delete:
          'Data hry jsem, bohužel, nesmazal, protože se mi nepodařilo připojit k serveru.',
      },
      email: {
        empty: 'E-mail musí být zadán!',
        form: 'E-mail nemá správný tvar!',
      },
      nick: {
        empty: 'Přezdívka musí být zadána!',
      },
      answer: {
        empty: 'Odpověď musí být zadána!',
      },
    },
    /*
    Informace v patičce
    */
    info:
      'Uložená data (e-mail a informace o stavu hry) se automaticky smažou po dokončení hry, po kliknutí na tlačítko Začít od začátku nebo po uplynutí jednoho roku od poslední správné odpovědi.',
  /*
  Generátor hesla
  */
    password: {
      name: 'Generátor hesla',
      placeholder: 'Napiš tajné heslo',
      label: 'Tajné heslo pro statistiky',
      empty: 'Heslo musí být zadáno!',
      send: "Zašifrovat heslo",
      hashLabel: 'Zašifrované heslo',
      hashPlaceholder: 'Heslo nebylo zatím zašifrováno',
      hashError: 'Heslo se nepodařilo zašifrovat!',
      copy: 'Zkopírovat zašifrované heslo do schránky',
      copySuccess: "Zašifrované heslo bylo úspěšně zkopírováno do schránky.",
      copyError: "Zašifrované heslo se nepovedlo zkopírováno do schránky!",
    },
  /*
  Statisiky
  */
    stats: {
      name: 'Statistiky',
      demoMode: 'demo mód',
      demoModeInfo: 'V demo módu se zobrazují skutečné statistiky, ale hráče nepůjde doopravdy smazat.',
      send: 'Získat statistiky',
      error: {
        general: 'Statistiky se nepodařilo vygenerovat. Je tam někde problém!',
        missing: 'Nebylo zadáno heslo!',
        denied: 'Přístup ke statistikám není povolen!',
        bad: 'Heslo není správné!',
        nodata: 'Nebyla získána žádná data!',
        delete: 'Hráče se nepodařilo smazat!',
      }
    },
  /*
  Tabulka s aktivními hráči
  */
    users: {
      title: "Aktuálně hrající hráči",
      delete: 'Smazat',
      nick: 'Přezdívka',
      currentLevel: 'Aktuální úroveň',
      lastAnswerNumber: 'Pořadí poslední zodp. otázky',
      lastAnswerDate: 'Datum odpovězení posl. otázky',
      nothing: 'Aktuálně hru nikdo nehraje.',
      deleteAlert: 'Doopravdy chceš smazat hráče s přezdívkou {nick}?',
      cancel: 'Zrušit',
      confirm: 'Smazat hráče',
      deleteResult: {
        true: 'Hráč byl úspěšně smazán.',
        false: 'Hráče se nepodařilo smazat!',
      },
    },
  /*
  Grafy
  */
    charts: {
      title: 'Statistiky odpovědí od {from} do {to}',
      refresh: 'Obnovit statistiky',
      wordsOccurrence: 'Celkové počty odpovědí',
      wordsPerDay: 'Počet odpovědí za den',
      uniqueWordsPerDay: 'Počet jedinečných odpovědí za den',
      otherWords: 'Další odpovědi',
      otherDays: 'Další dny',
      wordCount: 'počet odpovědí',
      nothing: 'Není tu nic k zobrazení…',
      error: {
        outOfRangeMessage: 'Datum je mimo povolený rozsah!',
        overlappingDatesMessage: 'Začátek musí být dřív než konec!',
        invalidDateMessage: 'Špatné datum!',
      },
    },
  },
  en: {
    /*
    Application name
    */
    name: 'Answer responder – demo',
    /* Application settings */
    setTranslation: 'Set application language',
    darkMode: 'Dark mode',
    deleteSettings: 'Delete app settings',
    personalDataDeleted: 'All personal data was deleted.',
    /*
    Button names
    */
    button: {
      sendAnswer: 'Submit answer',
      loadGame: 'Load game',
      startFromZero: 'Start from the beginning',
    },
    /*
    Form
    */
    form: {
      /*
      Email field
      */
      email: {
        label: '* Your e-mail address',
        placeholder: 'john.doe@example.com',
      },
      /*
      Nickname field
      */
      nick: {
        label: '* Your nickname',
        placeholder: 'nickname (for example from geocaching.com)',
      },
      /*
      Answer field
      */
      answer: {
        label: '* Your answer',
        placeholder: 'answer',
      },
    },
    /*
    Game tasks
    */
    task: {
      /*
      Only the first task, the other tasks get texts from the API
      */
      first: {
        title: 'Welcome, player!',
        hints: `This is a demo version of the game, which you can download and then (while keeping the information from the footer) use for your own game.
          You will learn each task (except the first one) from the e-mail that is sent to your e-mail box, as well as instructions after the last correctly answered task.

          This demo version has a total of six questions, which will be offered in three levels. In the last two levels, the question will be selected randomly.

          The first task (which you will usually learn in another way) is as follows:
          Queen had four members in the original lineup. Write the name of just one of them!

          First try to answer completely wrong, then try to write the wrong Freddie's name, for example Farok.
          The correct answers are:
          Freddie, Mercury, Freddie Mercury, Farrokh, Bulsara, Farrokh Bulsara
          Brian, May, Brian May, Brian Harold May
          John, Deacon, John Deacon\nRoger, Taylor, Roger Taylor, Roger Meddows Taylor.
          Try some of the correct answers, those with full names have their own special answer.
          All the answers other than those given here are wrong.

          Please write your e-mail address in the e-mail field, enter your nickname in the nickname field (for example from geocaching.com) and write your answer number %level% in the reply field.
          Then click the Submit answer button.
          If you have a game played, enter your e-mail and click on the Load game button.

          More on the website designed for this game (see footer).`,
      },
      /*
      Bad answer
      */
      badAnswerHeader: "Bad answer!",
    },
    /*
    Game recovery window
    */
    recovery: {
      title: 'Game recovery',
      info: `It looks like you've played the game before, but I couldn't restore it.\nPlease enter your recovery password that you received in the first email.
        After entering the password, you can resume the game and continue, or delete the game and start from the beginning.
        If you don't know the password, close this tab and start another e-mail or contact the author of the game.`,
      label: '* Your recovery password',
      placeholder: 'recovery password',
      errorEmptyPassword: 'Recovery password must be entered!',
      close: 'Close',
      delete: 'Delete game',
      recover: 'Continue the game',
      show: 'Show',
      hide: 'Hide',
      showHidePassword: '{showHide} password',
    },
    /*
    Error messages
    */
    error: {
      noBackend:
        "Failed to connect to server. It probably won't work today. Please write to the author of the game and describe what you did!",
      backend:
        "It probably won't work today, I can't connect. Please describe to the author of the game what you are doing!",
      backendHeader: 'Ooops, something happened because I found a problem…',
      recovery: {
        recover:
          "I couldn't restore the game because I couldn't connect to the server.",
        delete:
          'Unfortunately, I did not delete the game data because I could not connect to the server.',
      },
      email: {
        empty: 'Email must be entered!',
        form: 'Email is not in the correct format!',
      },
      nick: {
        empty: 'Nickname must be entered!',
      },
      answer: {
        empty: 'Answer must be entered!',
      },
    },
    /*
    Information in the footer
    */
    info:
      'Saved data (email and game status information) will be automatically deleted when the game is completed, when you click the Start from the beginning button, or one year after the last correct answer.',
  /*
  Password generator
  */
    password: {
      name: 'Password generator',
      placeholder: 'Type secret password',
      label: 'Secret password for statistics',
      empty: 'Password must be entered!',
      send: "Encrypt password",
      hashLabel: 'Encrypted password',
      hashPlaceholder: 'The password has not yet been encrypted',
      hashError: 'Password could not be encrypted!',
      copy: 'Copy the encrypted password to the clipboard',
      copySuccess: "The encrypted password was successfully copied to the clipboard.",
      copyError: "Encrypted password failed to be copied to clipboard!",
    },
    /*
    Statistics
    */
    stats: {
      name: 'Statistics',
      demoMode: 'demo mode',
      demoModeInfo: 'In demo mode, the actual statistics are displayed, but the player cannot be really deleted.',
      send: 'Get statistics',
      error: {
        general: 'Failed to generate statistics. There is a problem somewhere!',
        missing: 'No password entered!',
        denied: 'Access to statistics is not allowed!',
        bad: 'The password is incorrect!',
        nodata: 'No data obtained!',
        delete: 'Failed to delete player!',
      }
    },
    /*
    Table with active players
    */
    users: {
      title: "Currently playing players",
      delete: 'Delete',
      nick: 'Nick',
      currentLevel: 'Current level',
      lastAnswerNumber: 'Order of the last answ. question',
      lastAnswerDate: 'Last question answer date',
      nothing: 'Currently no one is playing the game.',
      deleteAlert: 'Doopravdy chceš smazat hráče s přezdívkou {nick}?',
      cancel: 'Cancel',
      confirm: 'Delete player',
      deleteResult: {
        true: 'The player was successfully deleted.',
        false: 'Failed to delete player!',
      },
    },
    /*
    Charts
    */
    charts: {
      title: 'Answer statistics from {from} to {to}',
      refresh: 'Rebuild statistics',
      wordsOccurrence: 'Total number of answers',
      wordsPerDay: 'Number of answers per day',
      uniqueWordsPerDay: 'Number of unique answers per day',
      otherWords: 'Other answers',
      otherDays: 'Other days',
      wordCount: 'answer count',
      nothing: 'There is nothing to display…',
      error: {
        outOfRangeMessage: 'Date is out of range!',
        overlappingDatesMessage: 'The beginning must be before the end!',
        invalidDateMessage: 'Bad date!',
      },
    },
  },
};
