module.exports = {
  extends: ['airbnb-typescript'],
  parserOptions: {
    project: './tsconfig.dev.json',
  },
  env: {
    browser: true,
    node: true,
  },
  rules: {
    '@typescript-eslint/no-unused-vars': 'warn',
    'no-debugger': 'warn',
    "no-console": ["warn", { allow: ["warn", "error"] }]
  },
  globals: {
    "PRODUCTION": true,
  }
};
