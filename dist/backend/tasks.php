<?php
/*
Seznam úkolů
Vždy se jako první odešle první úkol (u prvního úkolu stačí jako otázka znak pomlčka, protože tuto otázku se hráč dozví jinde).
Správných odpovědí může být více, odpovědi nejsou citlivé na velikost písmen.
Volitelně můžeš zadat možné špatné odpovědi, po jejichž zadání uvidí hráč odpovídající nápovědný text.
Volitelně také můžeš zadat doprovodné texty ke správným odpovědím. Pokud nezadáš ke správným odpovědím jejich doplňující
text, použije se univerzální text z překladů.
Pokud nepotřebuješ anglické otázky, smaž vše od „en => [“ až po „],“ (se stejným odsazením od levého kraje).
Každá otázka se vytváří podle následujícího vzoru:
new Task(
  "Otázka, která se hráči pošle na jeho e-mail (vyjma první otázky).",
  [
    "Jedna nebo více správných odpovědí v uvozovkách, oddělené čárkou.",
    "Nebere se ohled na malá a velká písmenka; mezery na začátku a na konci se oříznou a mezi slovy se zkrátí na jednu."
  ],
  0, Celé číslo, které určuje úroveň, pro kterou je otázka určena. Pouze první úkol může mít nastavenu nulu. Pokud nemají být úlohy předkládány podle aktuální úrovně hry (nastavuje se v souboru gameconfig.php) nebo nejsou úkoly předkládány náhodně, může být zapsáno libovolné číslo.
  [
    "nepovinná špatná odpověď" => "Nápovědný tekt k této odpovědi.",
    "jiná nepovinná špatná odpověď" => "Nápovědný text k této jiné odpovědi.",
  ],
  [
    "Volitelný doprovodný text k první správé odpovědi.",
    "Volitelný doprovodný text k druhé správé odpovědi.",
    "Pokud napíšeš %answer%, tak se nahradí odpovědí hráče, znak \n vytvoří zalomení řádku.",
  ],
),
*/
/*
Task list
The first task is always sent first (for the first task, a dash is enough as a question, because the player learns this question elsewhere).
There may be more than one correct answer, the answers are not case sensitive.
Optionally, you can enter possible incorrect answers, after which the player will see the corresponding help text.
Optionally, you can also enter accompanying texts for the correct answers. If you do not enter additional text to the correct answers,
the universal text from the translations will be used.
If you don't need English questions, delete everything from "en => [" to "]," (with the same indent from the left margin).
Each question is created according to the following pattern:
new Task(
  "A question that is sent to the player's e-mail (except for the first question).",
  [
    "One or more correct answers in quotation marks, separated by commas.",
    "Lowercase and uppercase letters are not taken into account; the spaces at the beginning and end are truncated and the spaces between the words are shortened to one."
  ],
  0, An integer that specifies the level for which the question is intended. Only the first task can be set to zero. If tasks are not to be submitted according to the current level of the game (set in the gameconfig.php file) or tasks are not submitted randomly, any number can be entered.
  [
    "optional wrong answer" => "A hint to this answer.",
    "another optional wrong answer" => "A hint to this other answer.",
  ],
  [
    "Optional accompanying text to the first response message",
    "Optional accompanying text to the second response message",
    "If you type %answer%, it will be replaced by the player's answer, the \n will create a line break.",
  ],
),
*/
$gameTasks = [
  "cs" => [
    new Task(
      "-",
      [
        "Farrokh Bulsara", "Roger Meddows Taylor", "Brian Harold May", "Freddie Mercury",
        "Roger Taylor", "Brian May", "John Deacon", "Farrokh", "Bulsara", "Freddie", "Mercury",
        "Brian", "Roger", "John", "May", "Taylor", "Deacon", "Harold", "Meddows",
        "Bulsara Farrokh", "Mercury Freddie",
        "Taylor Roger", "Taylor Roger Meddows", "Taylor Meddows Roger", "Deacon John",
        "May Brian", "May Brian Harold", 'May Harold Brian',
      ],
      0,
      [
        "Farokh" => "Těsně vedle, chybí ti jedno r.",
        "Farrok" => "Těsně vedle, chybí ti písmenko h.",
        "Farok" => "Těsně vedle, chybí ti písmenka r a h.",
      ],
      [
        "Správně, je vidět, že znáš Freddieho původní jméno!", // Farrokh Bulsara
        "Správně, %answer% je skutečně celé jméno bubeníka kapely Queen!", // Roger Meddows Taylor
        "Výborně, jeden z nejlepších kytaristů se celým jménem skutečně jmenuje %answer%!", // Brian Harold May
        "Ano, %answer% je správně,\nale víš, že se původně jmenoval Farrokh Bulsara?", // Freddie Mercury
        "Dobře, celé jméno tohoto důstojníka Řádu Britského impéria je Roger Meddows Taylor!", // Roger Taylor
        "Odpověděl jsi správně. Vystudovaný astrofyzik se celým jménem jmenuje Brian Harold May.", // Brian May
        "%answer% je jedna ze správných odpovědí. %answer% je například autorem písničky I Want To Break Free.", // John Deacon
        "Farrokh je původní křestní jméno Freddieho Mercuryho. Jeho původní příjmení je Bulsara.",
        "Bulsara je původní příjmení Freddieho Mercuryho. Jeho původní křestní jméno je Farrokh.",
        "Freddie je umělecké křestní jméno frontmana kapely Queen.\nPříjmením se jmenoval Mercury a jeho původní jméno je Farrokh Bulsara.",
        "Mercury je umělecké příjmení frontmana kapely Queen.\nJeho křestní jméno bylo Freddie a jeho původní jméno je Farrokh Bulsara.",
        "%answer% je křestní jméno kytaristy kapely Queen. Jeho celé jméno je Brian Harold May.",
        "%answer% je křestní jméno bubeníka kapely Queen. Jeho celé jméno je Roger Meddows Taylor.",
        "%answer% je křestní jméno basáka kapely Queen. Jeho celé jméno je John Deacon.",
        "%answer% je příjmení kytaristy kapely Queen. Jeho celé jméno je Brian Harold May.",
        "%answer% je příjmení bubeníka kapely Queen. Jeho celé jméno je Roger Meddows Taylor.",
        "%answer% je příjmení basáka kapely Queen. Jeho celé jméno je John Deacon.",
        "%answer% je příjmení basáka kapely Queen. Jeho celé jméno je John Deacon.",
        "%answer% je prostřední jméno kytaristy kapely Queen. Jeho celé jméno je Brian Harold May.",
        "%answer% je prostřední jméno bubeníka kapely Queen. Jeho celé jméno je Roger Meddows Taylor.",
      ],
    ),
    new Task(
      'Jak se jmenuje souputník planety Země?
      Můžeš zkusit kteroukoliv planetu naší sluneční soustavy a dostaneš vždy speciálně vytvořenou odpověď.
      Správná odpověď je Měsíc i Luna.
      ',
      ["Měsíc", "Luna"],
      1,
      [
        "Merkur" => "Ale kdepak, Merkur je sice nejblíž ke slunci, ale souputník Země to není.",
        "Venuše" => "Nikoliv. Venuše je sice jasná „hvězda“, kterou můžeš vidět ráno či večer, ale ta to není.",
        "Mars" => "Rudá planeta Mars to není, kdepak.",
        "Jupiter" => "Jupiter má sice velkou skvrnu, ale souputníkem Země není.",
        "Saturn" => "Tenhle plynný obr je znám svými krásnými prstenci, ale správnou odpovědí není.",
        "Uran" => "Uran to není. Je zajímavý tím, že póly má tam, kde jiné planety rovník.",
        "Neptun" => "Neptun je nejvzdálenější planeta od Země, která je také modrá jako Země.\nJejí souputník to ale není.",
      ],
    ),
    new Task(
      'Co připomíná tvar jizvy na Harryho čele?
      Jen pro jistotu: správná odpověď je blesk.
      ',
      ["blesk"],
      1,
    ),
    new Task(
      'Jak se jmenuje manželka Johna Lennona?
      Nejdřív zkus tyhle špatné odpovědi: Joko, Joko Ono, Ono Joko.
      Správných odpovědí je víc, Yoko, Ono, Yoko Ono i Ono Yoko.
      ',
      ["Yoko", "Ono", "Yoko Ono", "Ono Yoko"],
      2,
      [
        "Joko" => "Těsně vedle, jedno písmenko je špatně!",
        "Joko Ono" => "Těsně vedle, jedno písmenko je špatně!",
        "Ono Joko" => "Těsně vedle, jedno písmenko je špatně!",
      ]
    ),
    new Task(
      'Jak se jmenuje hlavní město USA?
      Zkus New York, ale správně je Washington.',
      ["Washington"],
      2,
      [
        "New York" => "New York je velké město, ale ne hlavní město USA",
      ]
    ),
    new Task(
      'Jak se říká pramenu, který je charakteristický nepravidelným únikem vody vyvrhované turbulentně (vířivě) do okolí a doprovázené vodní parou?
      Jediná správná odpověď je gejzír.',
      ["Gejzír"],
      2,
    ),
  ],
  "en" => [
    new Task(
      "-",
      [
        "Farrokh Bulsara", "Roger Meddows Taylor", "Brian Harold May", "Freddie Mercury",
        "Roger Taylor", "Brian May", "John Deacon", "Farrokh", "Bulsara", "Freddie", "Mercury",
        "Brian", "Roger", "John", "May", "Taylor", "Deacon", "Harold", "Meddows",
        "Bulsara Farrokh", "Mercury Freddie",
        "Taylor Roger", "Taylor Roger Meddows", "Taylor Meddows Roger", "Deacon John",
        "May Brian", "May Brian Harold", 'May Harold Brian',
      ],
      0,
      [
        "Farokh" => "Right next door, you miss one r.",
        "Farrok" => "Right next door, you're missing the letter h.",
        "Farok" => "Right next door, you're missing the letters r and h.",
      ],
      [
        "That's right, you know Freddie's original name!", // Farrokh Bulsara
        "That's right,% answe% is really the full name of the drummer of the Queen!", // Roger Meddows Taylor
        "Well, one of the best guitarists with the full name is really %answer%!", // Brian Harold May
        "Yes, %answer% is correct,\n but you know it was originally called Farrokh Bulsara?", // Freddie Mercury
        "Well, the full name of this British Empire officer is Roger Meddows Taylor!", // Roger Taylor
        "You answered correctly. But Brian Harold May is also a good astrophysicist.", // Brian May
        "%answer% is one of the correct answers. For example,%answer% is the author of the song I Want To Break Free.", // John Deacon
        "Farrokh is the original first name of Freddie Mercury. His original surname was Bulsara.",
        "Bulsara is the original surname of Freddie Mercury. His original first name was Farrokh.",
        "Freddie is the artistic first name of the frontman of the band Queen.\nThe last name was Mercury and his original name was Farrokh Bulsara.",
        "Mercury is the artistic surname of the frontman of the band Queen.\nHis first name was Freddie and his original name was Farrokh Bulsara.",
        "%answer% is the first name of the guitarist of the band Queen. His full name is Brian Harold May.",
        "%answer% is the first name of the drummer of the band Queen. His full name is Roger Meddows Taylor.",
        "%answer% is the first name of the bassist of the band Queen. His full name is John Deacon.",
        "%answer% is the last name of the guitarist of the band Queen. His full name is Brian Harold May.",
        "%answer% is the last name of the drummer of the band Queen. His full name is Roger Meddows Taylor.",
        "%answer% is the last name of the bassist of the band Queen. His full name is John Deacon.",
        "%answer% is the middle name of the guitarist of the band Queen. His full name is Brian Harold May.",
        "%answer% is the middle name of the drummer of the band Queen. His full name is Roger Meddows Taylor.",
      ],
    ),
    new Task(
      "What is the name of the companion of the planet Earth?
      You can try any planet in our solar system and you will always get a specially crafted answer.
      The correct answer is the Moon and Luna.",
      ["Moon", "Luna"],
      1,
      [
        "Mercury" => "Oh no, Mercury is closest to the sun, but it is not the companion of the Earth.",
        "Venus" => "Not. Venus is a bright \"star\" that you can see in the morning or evening, but it is not.",
        "Mars" => "It's not the red planet Mars, no way.",
        "Jupiter" => "Jupiter has a big spot, but he is not a companion to Earth.",
        "Saturn" => "This gas giant is known for its beautiful rings, but it is not the right answer.",
        "Uranus" => "It's not Uranus. It is interesting in that it has poles where other planets have an equator.",
        "Neptune" => "Neptune is a more distant planet from Earth, which is also as blue as Earth.\nBut it is not her companion.",
      ],
    ),
    new Task(
      "What does the shape of the scar on Harry's forehead look like?
      Just to be sure: the right answer is lightning.",
      ["lightning"],
      1,
    ),
    new Task(
      "What is the name of John Lennon's wife?
      Try these wrong answers first: Joko, Joko Ono, Ono Joko.
      There are more correct answers, Yoko, Ono, Yoko Ono and Ono Yoko.",
      ["Yoko", "Ono", "Yoko Ono" . "Ono Yoko"],
      2,
      [
        "Joko" => "Right next door, one letter is wrong!",
        "Joko Ono" => "Right next door, one letter is wrong!",
        "Ono Joko" => "Right next door, one letter is wrong!",
      ]
    ),
    new Task(
      "What is the name of the US capital?
      Try New York, but Washington is right.",
      ["Washington"],
      2,
      [
        "New York" => "New York is a big city, but it is not the capital of the USA",
      ]
    ),
    new Task(
      "What is the name of the spring, which is characterized by an intermittent discharge of water ejected turbulently and accompanied by steam?
      The only correct answer is a geyser.",
      ["Geyser"],
      2,
    ),
  ],
];
