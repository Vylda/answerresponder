<?php
require_once './cors.php';
require_once './connection.php';

$email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
$token = filter_input(INPUT_POST, "token", FILTER_SANITIZE_STRING);

// unauthorized access
if (empty($token)) {
  sendErrorMessage(intl("ERROR_UNAUTHORIZED_ANSWER"), 401);
}

// empty test
if (empty($email)) {
  $error = intl("ERROR_DELETE_DATA_EMAIL_MISSING");
  sendErrorMessage($error, 400);
}

try {
  $db->where('email', $email);
  $player = $db->getOne(TABLE_NAME);

  // user not found
  if (empty($player)) {
    sendErrorMessage(intl("ERROR_PLAYER_NOT_FOUND"), 404);
  }
  if ($token !== $player['token']) {
    sendRecoveryMode();
  }

  // delete player
  $db->where('email', $email);
  if ($db->delete(TABLE_NAME)) {
    sendRecoveryMode(false, null, null, intl("DELETE_DATA_SUCCESS"), createToken(), 200);
  } else {
    sendErrorMessage(intl("ERROR_DELETE_ACCOUNT_FAILED"));
  }
} catch (\Exception $e) {
  sendErrorMessage($e->getMessage());
}
