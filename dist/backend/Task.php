<?php
require_once "./common.php";
require_once "./gameconfig.php";

/**
 * Task class
 */
class Task
{
  private $question = "";
  private $answers = [];
  private $level = 0;
  private $answersMessages = [];
  private $errors = [];

  public function __construct($q, $a, $l, $errors = [], $am = [])
  {
    try {
      if (empty($q) || empty($a)) {
        throw new Exception(intl("ERROR_EMPTY_MANDATORY_VARS"));
      }
      if (!is_array($a)) {
        throw new Exception(intl("ERROR_ANSWERS_NOT_ARRAY"));
      }
      if (!empty($am) && !is_array($am)) {
        throw new Exception(intl("ERROR_ANSWER_MESSAGES_NOT_ARRAY"));
      }
      if (!empty($errors) && !is_array($errors)) {
        throw new Exception(intl("ERROR_ERROR_MESSAGES_NOT_ARRAY"));
      }
      if (!is_int($l)) {
        throw new Exception(intl("ERROR_LEVEL_IS_NOT_INT"));
      }
    } catch (Error $e) {
      sendErrorMessage($e->getMessage());
    }

    // clean data
    $this->question = trim($q);
    $this->answers = array_map(function ($ans) {
      return mb_strtolower(trim($ans));
    }, $a);

    $this->level = $l;

    if (!empty($errors)) {
      foreach ($errors as $key => $val) {
        $this->errors[trim(mb_strtolower($key))] = trim($val);
      }
    }
    if (!empty($am)) {
      foreach ($am as $key => $val) {
        $this->answersMessages[$key] = trim($val);
      }
    } else {
      $this->answersMessages = [intl("CORRECT_ANSWER")];
    }
  }

  /**
   * Gettter
   * @param mixed $name
   * @return string|array|null
   */
  public function __get($name)
  {
    switch ($name) {
      case "question":
        return $this->question;
      case "answers":
        return $this->answers;
      case "level":
        return $this->level;
      default:
        return null;
    }
  }

  /**
   * Test if answer is correct
   * @param String $my_answer Answer from player
   * @return Correct
   */
  public function isCorrect($my_answer)
  {
    $answer = $this->mbTrim($my_answer);

    if (empty($answer)) {
      return new Correct(false, intl("NO_ANSWER"), null);
    }

    if (in_array($answer, $this->answers)) {
      $correctData = $this->getLevelAnswer($answer);
      return new Correct(
        true,
        replaceString($correctData["message"], $my_answer, "answer"),
        $correctData["lastAnswerIndex"],
      );
    }

    if (array_key_exists($answer, $this->errors)) {
      return new Correct(false, $this->errors[$answer], null);
    }

    return new Correct(false, replaceString(intl("INCORRECT_ANSWER"), $my_answer, "answer"), null);
  }

  private function mbTrim($string)
  {
    mb_regex_encoding("UTF-8");
    mb_internal_encoding("UTF-8");
    $smallAnswer = mb_strtolower($string);

    $whiteSpaces = [
      "/\x{00A0}/u",
      "/\x{2000}/u",
      "/\x{2001}/u",
      "/\x{2002}/u",
      "/\x{2003}/u",
      "/\x{2004}/u",
      "/\x{2005}/u",
      "/\x{2006}/u",
      "/\x{2007}/u",
      "/\x{2008}/u",
      "/\x{2009}/u",
      "/\x{200A}/u",
      "/\x{200B}/u",
      "/\x{1680}/u",
      "/\x{180E}/u",
      "/\x{202F}/u",
      "/\x{205F}/u",
      "/\x{3000}/u",
      "/\x{FEFF}/u",
    ];

    $replacedAnswer = preg_replace(
      $whiteSpaces,
      " ",
      $smallAnswer
    );

    $replacedAnswer = preg_replace(
      "/\s+/",
      " ",
      $replacedAnswer
    );
    return trim($replacedAnswer);
  }

  /**
   * Get level and answer message by answer
   * @param String $answer
   * @return (int|string|false)[]
   */
  public function getLevelAnswer($answer)
  {
    $answerIndex = array_search($answer, $this->answers, true);
    $messageIndex = min(count($this->answersMessages) - 1, $answerIndex);
    return [
      "message" => $this->answersMessages[$messageIndex],
      "lastAnswerIndex" => $answerIndex,
    ];
  }
}

/**
 * Helper class with correct data
 */
class Correct
{
  private $isCorrect = false;
  private $message = '';
  private $lastAnswerId = null;

  public function __construct($isCorrect, $message, $lastAnswerId)
  {
    $this->message = $message;
    $this->isCorrect = $isCorrect;
    $this->lastAnswerId = $lastAnswerId;
  }

  public function __get($name)
  {
    switch ($name) {
      case "isCorrect":
        return $this->isCorrect;
      case "message":
        return $this->message;
      case "lastAnswerId":
        return $this->lastAnswerId;
      default:
        return null;
    }
  }
}
