<?php
/*
Domény (oddělené mezerou), ze které budou chodit požadavky na data
Main domains (separated by space), where is front-end located
*/
define("MAIN_DOMAINS", "http://answerresponder.cz https://answerresponder.cz http://www.answerresponder.cz https://www.answerresponder.cz");

/*
Informace o přístupu k databázi a tabulce s daty
Informations about database and data table access
*/
define("HOST", 'localhost');
define("USER", 'root');
define("PASSWORD", '');
define("DB_NAME", 'answerresponder');
define("TABLE_NAME", 'answerresponder');


/*
Nastavení statistik slov
Words stat settings
*/
define("ANSWERS_TABLE_NAME", 'answerresponder_answers');
/*
Zapnout zobrazení statistik
Turn on display statistics
*/
define("STATS_ENABLED", false);
/*
Heslo pro zobrazení statistik; lze vygenerovat na stránce password.html
The password for displaying statistics; can be generated on the password.html page
*/
define("STATS_PASSWORD", '$2y$10$bWRcLML9rKKAZfYI29NNqOobU6OtyKA.FDTm/2scTnw4H5NaKlqGa'); // statspassword
/*
Pokud je demo mód true, pak nelze uživatele smazat
If demo mode is true, delete user is not possible
*/
define("STATS_DEMO_MODE", false);
