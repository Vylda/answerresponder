<?php
// delete data older then 1 year
$dateTime = new DateTime();
$timestamp = $dateTime->format("Y-m-d H-i-s");
$yearAgo = $dateTime->modify('-1 year');
$yearAgoTS = $yearAgo->format("Y-m-d H-i-s");
$db->where('timestamp', $yearAgoTS, "<");
$db->delete(TABLE_NAME);
