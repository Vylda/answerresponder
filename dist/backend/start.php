<?php
require_once './cors.php';
require_once './connection.php';

/*
Test whether the player has already played the game. It usually starts when the page is reloaded.
*/

$email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
$token = filter_input(INPUT_POST, "token", FILTER_SANITIZE_STRING);


// no email, no token
if (empty($email) && empty($token)) {
  sendToken();
}

if (!empty($email)) {
  $db->where('email', $email);
  $gamePlayer = $db->getOne(TABLE_NAME);

  // player not saved, it is new player
  if (empty($gamePlayer)) {
    sendToken();
  }

  // token is correct
  if ($gamePlayer["token"] === $token) {
    $solvedQuestions = json_decode($gamePlayer["solved_questions"]);
    $sqCount = count($solvedQuestions);

    // some answered questions
    if ($sqCount > 0) {
      $questionIndex = $solvedQuestions[$sqCount - 1];

      // get last player question data
      $lastAnswerId = $gamePlayer["last_answer_id"];
      $tasks = intlTasks();
      if (empty($tasks)) {
        sendErrorMessage(intl("ERROR_NO_TASKS"));
      }
      $currentTask = $tasks[$questionIndex];
      $answer = $currentTask->answers[$lastAnswerId];
      $answerData = $currentTask->getLevelAnswer($answer);
      $message = replaceString($answerData["message"], $answer, "answer");
      $nick = $gamePlayer["nick"];

      sendGameLevelData($message, $gamePlayer["current_level"], $email, $nick);
    } else {
      sendNoTask(intl("NOTHING_ANSWERED"), $gamePlayer["nick"]);
    }
  }

  sendRecoveryMode();
}
