# Answer Responder

## TL;DR

Aplikace pro řešení otázek a odpovědí. Její obrovskou výhodou (oproti řešení založeném na automatických odpovědích na e-mailovém serveru) je ta, že aplikaci nikdo nezaspamuje a nestane se tak, že by přestala odpovídat (jak je tomu u veřejných e-mailových služeb), a hráč, byť by znal poslední správnou odpověď, musí projít všechny úkoly dané autorem hry. Datová komunikace (vyjma prvního načtení, kde se načte zhruba 450 kB dat) je velice úsporná - takže hra je vhodná i pro hraní na mobilních telefonech. Rovněž je vyloučena chybná interpretace odpovědi na e-mailovém serveru.

Demo hry si můžeš zahrát na webu [AnswerResponder.cz](https://answerresponder.cz).

## Princip hry

Hráč přijde na stránku s aplikací a zadá svůj e-mail, přezdívku a odpověď na první otázku (tu se hráč obvykle dozví z jiného zdroje spolu s webovou adresou hry). Pokud je odpověď správná, postupuje do další úrovně. Otázku pro následující úroveň, stejně jako informace po skončení hry, dostane vždy na svůj zadaný e-mail. Tak pokračuje až do konce hry.

Pokud hráč přeruší hru a začne ji kdykoliv znovu hrát, může (v případě, že bude hrát v jiném prohlížeči nebo v prohlížeči, kde vymazal osobní data, musí znovu zadat e-mail a obnovovací heslo) načíst uloženou hru a to až do uplynutí lhůty jeden rok od poslední správné odpovědi. Pak musí vždy začít hrát od začátku. Hru také může kdykoliv začít hrát od začátku.

Hru si může kdokoliv stáhnout a použít na svém serveru zcela bezplatně (při zachování licence, resp. patičky) nebo ji lze hostovat na webové adrese XXXXX.answerResponder.cz. V tom případě kontaktuj autora aplikace.

Hra je konfigurovatelná, úkolů můžeš mít kolik chceš, lze je projít všechny nebo jen část z nich, úkoly se mohou volit náhodně nebo je můžeš projít postupně jeden za druhým. Při náhodném procházení úkolů můžeš stanovit, v jaké úrovni hry se mají použít.

Hru (otázky i aplikaci) lze lokalizovat do libovolného jazyka, volba jazyka se děje automaticky na základě nastaveného jazyka pro procházení stránek v prohlížeči. Pokud překlad v daném jazyce neexistuje, použije se výchozí (obvykle čeština).

### Možné varianty hraní hry

Ve hře je třeba mít zadánu alespoň jednu nebo více otázek. Každá z otázek musí mít jednu nebo více správných odpovědí. Na každou ze správných odpovědí lze mít svůj vlastní potvrzovací text, který se v aplikaci objeví, pokud je odpověď správná. Otázka může mít definované nejčastěji se opakující špatné odpovědi s nápovědným textem (například překlepy).

Otázky lze předkládat náhodně nebo je lze klást postupně v pořadí, v jakém byly zadány. Lze také definovat, kolik otázek musí hráč správně zodpovědět.

Při náhodném předkládání lze stanovit, ve které úrovni hry se otázka může objevit.

První otázka v seznamu otázek se vždy zodpovídá jako první.

### Statistiky

V případě potřeby lze zapnout sledování statistik hráčů a použitých slov. Pro přístup ke statistikám je třeba povolit jejich zobrazení a znát správné heslo.

Pak je možné sledovat základní údaje o právě hrajících hráčích (a v případě potřeby jejich data smazat) a některé statistiky odpovědí.

Jak takové statistiky vypadají se můžeš podívat na demoverzi statistik na [stránce statistik Answer Responderu](https://answerresponder.cz/stats.html). Heslo je **statspassword**.

### Data hry

Data hry jsou uložena pro pokračování ve hře. Data se automaticky smažou rok po poslední správné odpovědi nebo po dokončení hry. Hráč může data smazat taky sám a hru začít hrát od začátku.

## Podmínky pro hostování hry na vlastním serveru

Hru lze nainstalovat na server s touto minimální konfigurací běhového prostředí

- PHP 7.3
- mySQL 8
- Apache 2.4.46

## Jednoduché spuštění

**Pozor! Při úpravách si dej pozor na uvozovky, závorky a čárky, jakákoliv chyba může způsobit, že hra přestane fungovat!**

**Pozorně si přečtěte komentáře u jednotlivých řádků v upravovaných souborech!**

**Pro vytvoření souboru s úkoly použijte [Task Creator](https://vylda.gitlab.io/task-creator).**

1. Stáhni si [archiv zip se soubory aplikace](https://gitlab.com/Vylda/answerresponder/-/archive/master/answerresponder-master.zip?path=dist) na svůj počítač a rozbal jej.
2. Vstup do složky `dist`.
3. V souboru `backend/tasks.php` zadej všechny otázky. Pokyny k zadávání otázek najdeš v tomto souboru. Soubor si také můžeš vytvořit na webu [Task Creator](https://vylda.gitlab.io/task-creator).
4. Otevři a uprav parametry hry v následujících souborech:
   - `config.js`
   - `backend/gameconfig.js`
5. Ve stejných souborech uprav i překlady. Pokud nepotřebuješ nějakou jazykovou mutaci, můžeš ji smazat, nebo, v případě potřeby, přidat novou.
6. Uprav soubor `backend/config.php`. Vlož data pro přístup k databázi (sdělí ti je poskytovatel tvé databáze) a uprav domény, na kterých poběží tvá aplikace.
7. Nakopíruj všechny soubory a složky ze složky `dist` na produkční server (obvykle přes FTP; jak na to ti řekne poskytovatel tvého webového prostoru).
8. Spusť soubor `backend/instaldb.php` nebo naimportuj do tvé produkční databáze (přístup ti předá provozovatel tvého databázového serveru) soubor `answerresponder.sql` . Tím v ní vytvoříš tabulku pro data aplikace.
9. Ve složce `dist/backend` na serveru smaž soubor `installdb.php`. Pokud ho tam necháš, může ti potencionální útočník smazat data všech hrajících hráčů.
10. Mělo by to fungovat.
11. V nouzi nejvyšší kontaktuj autora hry.

### Spuštění statistik

1. Na stránce `password.html` na tvém serveru zadej heslo pro přístup do statistik (co nejsilnější, protože se pak dají mazat rozehrané hry a budou vidět všechny odpovědi – špatné i správné) a vygeneruj si zašifrované heslo (je to bezpečné, heslo se samo nikam neuloží).
2. Vygenerované zašifrované heslo zkopíruj (na stránce je na to tlačítko) do souboru `backend/config.php` na řádek 33 (tam kde je definováno `STATS_PASSWORD`; přepiš současnou hodnotu).
3. Změň na řádku 28 souboru `backend/config.php` hodnotu `STATS_ENABLED` na `true`. Tato hodnota povolí přístup ke statistikám.
4. Pro přístup do statistik zadej na stránce `stats.html` své heslo.

## Instalace pro vývojáře

### Instalace vývojového prostředí

Nejdříve je třeba nainstalovat poslední verzi [Node.js](https://nodejs.org/en/download/).

Následně si vyklonuj repozitář přes git

```bash
git clone git@gitlab.com:Vylda/answerresponder.git
```

nebo si stáhni soubor [zip se zdrojovými kódy](https://gitlab.com/Vylda/answerresponder/-/archive/master/answerresponder-master.zip), pak rozbal složku **answerresponder-master** na disk a přejmenuj ji na **answerresponder**.

Následně spusť instalaci potřebných souborů:

```bash
cd answerresponder
npm install
```

### Vývoj

Nakopíruj soubory ze složky `backend` do složky tvého lokálního PHP serveru, naimportuj data pro pro tabulku ze souboru `answerresponder.sql` do lokální databáze a uprav soubory `backend/config.php` (ve složce lokálního PHP serveru) a `config.js` (ve složce `static`).

Pokud jsi ve složce se zdrojovými kódy, můžeš pro vývoj spustit vývojový server (vedle PHP a databázového serveru) s hlídáním změn:

```bash
npm start
```

nebo můžeš vytvořit vývojářskou verzi aplikace do složky `dist`

```bash
npm run dev
```

Pokud potřebuješ vytvořit verzi aplikace pro produkční web, spusť příkaz

```bash
npm run build
```

a nastav v příslušných souborech (viz [Jednoduché spuštění](#jednoduché-spuštění)) překlady, nastavení hry a informace o databázi. Toto můžeš udělat i před vytvořením produkční verze aplikace. Nakonec vytvoř produkční verzi aplikace příkazem `npm run build` a následně zkopíruj všechny soubory a složky ze složky `dist` na produkční server.

## Licence

Hru a její zdrojové kódy je možno používat, šířit a modifikovat na základě licence [Creative Commons (BY-NC-SA)](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.cs).

### TL;DR

- Uveď původ (BY)
- Neužívej komerčně (NC)
- Zachovej licenci (SA)
