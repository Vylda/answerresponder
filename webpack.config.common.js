const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: './src/index.tsx',
  entry: {
    index: './src/index.tsx',
    password: './src/password.tsx',
    stats: './src/stats.tsx',
  },
  target: 'web',
  output: {
    filename: '[name].[contenthash].js',
    path: path.resolve(__dirname, 'dist'),
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.(css|less)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 2,
            },
          },
          'postcss-loader',
          'less-loader',
        ],
      },
      {
        test: /\.(jpg|eot|woff|woff2|svg|ttf)([\?]?.*)$/,
        use: ['file-loader'],
      },
      {
        test: /\.(png|woff|woff2|eot|ttf)$/,
        use: ['url-loader?limit=100000'],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: 'Answer responder',
      template: './template/index.html',
      chunks: ['index'],
    }),
    new HtmlWebpackPlugin({
      title: 'Answer responder – password generator',
      filename: 'password.html',
      template: './template/index.html',
      chunks: ['password'],
    }),
    new HtmlWebpackPlugin({
      title: 'Answer responder – statistics',
      filename: 'stats.html',
      template: './template/index.html',
      chunks: ['stats'],
    }),
    new MiniCssExtractPlugin({
      filename: 'style.css',
    }),
    new CopyPlugin({
      patterns: [
        { from: './static', to: './' },
        { from: './backend', to: './backend' },
      ],
    }),
  ],
};
