const fs = require('fs');

const [minWordsPerDay, maxWordsPerDay] = [0, 300];
const totalDays = 450;
const words = ['shout', 'language', 'minister', 'blind', 'depend', 'prospect', 'hear',
  'improvement', 'missile', 'law', 'soul', 'hike', 'sentence', 'beard', 'career',
  'protect', 'production', 'transmission', 'wrestle', 'chocolate', 'overcharge',
  'ant', 'lend', 'we', 'budge', 'benefit', 'summer', 'attract', 'beneficiary',
  'referral', 'portrait', 'crisis', 'carpet', 'veteran', 'barrel', 'swarm',
  'raid', 'laundry', 'colony', 'helicopter', 'brilliance', 'pole', 'education',
  'grant', 'bond', 'hen', 'tap', 'separation', 'exotic', 'throat'];
const today = new Date();
let lineNumber = 1;

const outData = [];
for (let d = totalDays; d >= 0; d--) {
  const currentWordCount = Math.max(Math.floor(Math.random() * maxWordsPerDay), minWordsPerDay);
  const tempDate = new Date(today.getTime() - (d * 24 * 60 * 60 * 1000));
  for (let w = 0; w < currentWordCount; w++) {
    const wordNumber = Math.floor(Math.random() * words.length);
    const year = tempDate.getFullYear();
    const month = (tempDate.getMonth() + 1).toString().padStart(2, '0');
    const day = tempDate.getDate().toString().padStart(2, '0');
    const hours = Math.floor(Math.random() * 24).toString().padStart(2, '0');
    const minutes = Math.floor(Math.random() * 60).toString().padStart(2, '0');
    const seconds = Math.floor(Math.random() * 60).toString().padStart(2, '0');
    const date = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
    const values = `${lineNumber}, '${words[wordNumber]}', '${date}'`
    outData.push(`INSERT INTO \`answerresponder_words\` (\`word_id\`, \`word\`, \`date\`) VALUES(${values});`);
    lineNumber++;
  }
}

fs.writeFile('words.sql', outData.join('\n'), (err) => {
  if (err) throw err;
  console.log('The file has been saved!');
});
