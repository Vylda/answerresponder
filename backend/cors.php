<?php
require_once "./config.php";

// CORS headers
$domain = $domain = strtolower(filter_input(INPUT_SERVER, "HTTP_ORIGIN"));
$allowedDomains = preg_split("/\s+/", MAIN_DOMAINS);

$lowerCasedDomains = array_map(
  function ($defDomain) {return strtolower($defDomain);},
  $allowedDomains
);

if (!in_array($domain, $lowerCasedDomains)) {
  header('Access-Control-Allow-Origin:' . $allowedDomains[0]);
  die();
}

header('Access-Control-Allow-Origin:' . $domain);
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, X-Send-By, X-lang');

$method = filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING);
if ($method === 'OPTIONS') {
  die();
}

// request is from my app named ar-app
$app = filter_input(INPUT_SERVER, 'HTTP_X_SEND_BY', FILTER_SANITIZE_STRING);
$langCode = filter_input(INPUT_SERVER, 'HTTP_X_LANG', FILTER_SANITIZE_STRING);

require_once './common.php';
require_once './gameconfig.php';
// unauthorized access not from application
if (empty($app) || $app != "ar-app") {
  sendErrorMessage(intl("ERROR_UNAUTHORIZED_ANSWER"), 400);
}
