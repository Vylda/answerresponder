<?php
require_once './cors.php';
require_once './connection.php';

$email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
$password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
$mode = filter_input(INPUT_POST, "mode", FILTER_SANITIZE_STRING);

// empty test
if (empty($password)) {
  $error = intl("ERROR_DELETE_DATA_EMAIL_MISSING");
  sendRecoveryMode(true, $error, null, null, null, 400);
}
if (empty($email) || empty($mode)) {
  $error = intl("ERROR_NOT_ALL_DATA");
  sendRecoveryMode(true, $error, null, null, null, 400);
}

try {
  // deleta data from database
  $db->where('email', $email);
  $player = $db->getOne(TABLE_NAME);
  if (empty($player)) {
    $error = intl("ERROR_PLAYER_NOT_FOUND");
    sendRecoveryMode(false, null, $error, null, null, 200);
  }

  // verify password
  $goodPassword = password_verify($password, $player["recovery_password"]);
  if ($goodPassword) {
    // delete data
    $db->where('email', $email);
    if ($mode === "delete") {
      if ($db->delete(TABLE_NAME)) {
        sendRecoveryMode(false, null, null, intl("DELETE_DATA_SUCCESS"), createToken(), 200);
      } else {
        sendRecoveryMode(false, null, intl("ERROR_DELETE_ACCOUNT_FAILED"), null, null, 200);
      }
    } else if ($mode === "recover") {
      // recover data
      $newToken = createToken();
      $updateData = [
        "token" => $newToken,
      ];
      // table data update
      if ($db->update(TABLE_NAME, $updateData)) {
        sendRecoveryMode(false, null, null, intl("GAME_RECOVERY_SUCCESS"), $newToken, 200);
      } else {
        sendRecoveryMode(false, null, intl("ERROR_UPDATE"), null, null, 200);
      }
    } else {
      sendRecoveryMode(false, null, "ERROR_UNSUPORTED_RECOVERY_METHOD", null, null, 200);
    }
  }

  sendRecoveryMode(true, intl("ERROR_BAD_RECOVERY_PASSWORD"), null, null, null, 200);
} catch (\Exception $e) {
  sendRecoveryMode(false, null, intl("ERROR_RECOVERY_GAME_DATA"), null, null, 200);
}
