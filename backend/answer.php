<?php
require_once "./cors.php";
require_once "./connection.php";

$email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
$token = filter_input(INPUT_POST, "token", FILTER_SANITIZE_STRING);
$answer = filter_input(INPUT_POST, "answer", FILTER_SANITIZE_STRING);
$nick = filter_input(INPUT_POST, "nick", FILTER_SANITIZE_STRING);

// unathorized access
if (empty($token)) {
  sendErrorMessage(intl("ERROR_UNAUTHORIZED_ANSWER"), 401);
}

// no email or no answer
if (empty($email) || empty($answer)) {
  $missing = [];

  if (empty($email)) {
    array_push($missing, intl("ERROR_MISSING_FIELD_EMAIL"));
  }
  if (empty($answer)) {
    array_push($missing, intl("ERROR_MISSING_FIELD_ANSWER"));
  }

  $error = intl("ERROR_MISSING_FIELD") . ' ' . join(", ", $missing) . ".";
  sendErrorMessage($error, 400);
}
// main block
try {
  // test if user exists
  $db->where('email', $email);
  $player = $db->getOne(TABLE_NAME);

  $level = 0;
  $recoveryPassword = null;

  $newPlayer = empty($player);
  // empty player
  if ($newPlayer) {
    // no nickname
    if (empty($nick)) {
      $error = intl("ERROR_MISSING_FIELD") . ' ' . intl("ERROR_MISSING_FIELD_NICK") . ".";
      sendErrorMessage($error, 400);
    }

    // create recovery password
    $recoveryPassword = "";
    do {
      $recoveryPassword = preg_replace('/[[:^print:]]/', '', trim(bin2hex(random_bytes(6))));
    } while (empty($recoveryPassword));

    $hash = password_hash($recoveryPassword, PASSWORD_BCRYPT);

    // new player data
    $player = array(
      "email" => $email,
      "nick" => $nick,
      "current_level" => $level,
      "current_question_id" => 0,
      "solved_questions" => json_encode([]),
      "recovery_password" => $hash,
      "token" => $token,
    );
  }

  // is token same as in player's data
  if ($token !== $player["token"]) {
    sendRecoveryMode();
  }

  $nick = $player["nick"];

  //save answer to DB
  $word_to_save = array(
    "word" => $answer,
  );
  $db->insert(
    ANSWERS_TABLE_NAME,
    $word_to_save
  );

  //get task and test answer
  $tasks = intlTasks();
  if (empty($tasks)) {
    sendErrorMessage(intl("ERROR_NO_TASKS"));
  }

  $currentTask = $tasks[$player["current_question_id"]];
  $questionResult = $currentTask->isCorrect($answer);
  if (!$questionResult->isCorrect) {
    // answer is not correct
    sendAnswerErrorMessage($answer, $questionResult->message, 404);
    die();
  }

  if ($newPlayer) {
    // insert new player data to database
    $db->insert(TABLE_NAME, $player);
  }

  // answer is correct
  $mailer = configureMailer($email);
  $newLevel = $player["current_level"] + 1;

  // add question number to solved questions array
  $solvedQuestions = json_decode($player["solved_questions"]);
  array_push($solvedQuestions, $player["current_question_id"]);

  // get questions, who is not solved
  $unusedQuestions = array_filter(
    $tasks,
    function ($val) use ($solvedQuestions) {
      return !in_array($val, $solvedQuestions);
    },
    ARRAY_FILTER_USE_KEY,
  );

  if (RANDOMIZED && PROVIDE_TASKS_BY_LEVELS) {
    $unusedQuestions = array_filter(
      $unusedQuestions,
      function ($task) use ($newLevel) {
        return $task->level === $newLevel;
      },
    );
  }

  $unusedQuestionKeys = array_keys($unusedQuestions);

  // all answered
  if ($newLevel >= MAX_LEVEL || count($unusedQuestionKeys) == 0) {
    // delete player's data
    $db->where('email', $email);
    if ($db->delete(TABLE_NAME)) {
      // send final email
      $mailer->Subject = intl("MAIL_FINAL_SUBJECT");
      $mailer->Body = replaceString(intl("MAIL_FINAL_MESSAGE"), $nick, "nick");
      $mailer->send();

      // send messages to app
      $hints = replaceString(intl("APP_FINAL_MESSAGE"), $email, "e-mail");
      $hints = replaceString($hints, $nick, "nick");
      $result = [
        "level" => $newLevel,
        "title" => intl("APP_FINAL_TITLE"),
        "hints" => explode("\n", $hints),
        "completed" => true,
      ];
      response($result, 200);
    } else {
      sendErrorMessage(intl("ERROR_DELETE_DATA_GAME_END"));
    }
  }

  // new task
  $newQuestionKey = RANDOMIZED ? random_int(0, count($unusedQuestionKeys) - 1) : 0;
  $newQuestionIndex = $unusedQuestionKeys[$newQuestionKey];

  // texts to task
  $subject = getCurrentMessage(intl("MAIL_LEVEL_SUBJECTS"), max($newLevel - 1, 0));
  $messageBefore = getCurrentMessage(intl("MAIL_TASK_MESSAGES_BEFORE"), max($newLevel - 1, 0));
  $messageAfter = getCurrentMessage(intl("MAIL_TASK_MESSAGES_AFTER"), max($newLevel - 1, 0));

  // send next level task ba email
  $mailer->Subject = replaceString($subject, $newLevel + 1, "level");
  $body = replaceString($messageBefore, $newLevel + 1, "level");
  $body .= "\n" . $tasks[$newQuestionIndex]->question . "\n";
  $body .= replaceString($messageAfter, $newLevel + 1, "level");
  $body = replaceString($body, $nick, "nick");

  if ($player["current_level"] === 0) {
    $body .= "\n\n" . intl("RECOVERY_PASSWORD") . " " . $recoveryPassword;
  }
  $mailer->Body = $body;
  $mailer->send();

  // database update
  $updateData = array(
    "current_level" => $newLevel,
    "current_question_id" => $newQuestionIndex,
    "solved_questions" => json_encode($solvedQuestions),
    "last_answer_id" => $questionResult->lastAnswerId,
    "timestamp" => $timestamp,
  );
  $db->where('email', $email);

  if ($db->update(TABLE_NAME, $updateData)) {
    sendGameLevelData($questionResult->message, $newLevel, $email, $nick);
  } else {
    sendErrorMessage(intl("ERROR_UPDATE"));
  }
} catch (\Exception $e) {
  sendErrorMessage($e->getMessage(), 401);
}
