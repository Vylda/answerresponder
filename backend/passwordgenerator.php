<?php
require_once "./cors.php";
require_once "./connection.php";
require_once "./common.php";

$password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);

if (empty($password)) {
  response(["error" => "PASSWORD NEEDED"], 400);
}

$hash = password_hash($password, PASSWORD_BCRYPT);
response(["passwordHash" => $hash]);
