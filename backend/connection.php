<?php
require_once "./config.php";

// https://github.com/ThingEngineer/PHP-MySQLi-Database-Class
include_once 'db/MysqliDb.php';

$db = new MysqliDb(HOST, USER, PASSWORD, DB_NAME);

// delete old data from database
require_once("./deleteOldData.php");
