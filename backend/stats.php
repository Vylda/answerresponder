<?php
require_once "./cors.php";
require_once "./connection.php";
require_once "./common.php";

function filter_date_generator($value = '')
{
  $v = trim($value);
  $pattern = "/^(\d{4})-(\d{2})-(\d{2})$/";
  if (!preg_match($pattern, $value, $matches)) {
    return null;
  }
  $year = intval($matches[1]);
  $month = intval($matches[2]);
  $day = intval($matches[3]);
  if (!checkdate($month, $day, $year)) {
    return null;
  }
  return $v;
}

$password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
$startDate = filter_input(INPUT_POST, "startDate", FILTER_CALLBACK, array("options" => "filter_date_generator"));
$endDate = filter_input(INPUT_POST, "endDate", FILTER_CALLBACK, array("options" => "filter_date_generator"));
$emailToDelete = filter_input(INPUT_POST, "delete", FILTER_VALIDATE_EMAIL);

if (empty($password)) {
  response(["error" => "PASSWORD MISSING"], 401);
}
if (STATS_ENABLED !== TRUE) {
  response(["error" => "ACCESS DENIED"], 401);
}
if (!password_verify($password, STATS_PASSWORD)) {
  response(["error" => "BAD PASSWORD"], 401);
}

$endDateObj = empty($endDate) ? new DateTime() : new DateTime($endDate);
$endDateObj->setTime(23, 59, 59);

$startDateObj = empty($startDate) ? (clone $endDateObj)->modify('-14 day') : new DateTime($startDate);

if ($startDateObj > $endDateObj) {
  $startDateObj = (clone $endDateObj);
}
$startDateObj->setTime(0, 0, 0);
$endDateString = $endDateObj->format("Y-m-d H:i:s");
$startDateString = $startDateObj->format("Y-m-d H:i:s");

$deleteSuccess = null;

try {
  // delete user
  if (!empty($emailToDelete)) {
    $db->where("email", $emailToDelete);
    if (!STATS_DEMO_MODE) {
      if ($db->delete(TABLE_NAME)) {
        $deleteSuccess = $db->count > 0;
      } else {
        response("DELETE ERROR", 500);
      }
    } else {
      $db->get(TABLE_NAME);
      $deleteSuccess = $db->count > 0;
    }
  }

  $cols = array("word", "COUNT(1) as wordOccurrence");
  $db->where('date', array($startDateString, $endDateString), 'BETWEEN');
  $db->groupBy("word");
  $db->orderBy("wordOccurrence", "desc");
  $wordsStat = $db->get(ANSWERS_TABLE_NAME, null, $cols);

  $cols = array("DATE(date) as dayDate", "COUNT(word) as wordsOccurrence");
  $db->where('date', array($startDateString, $endDateString), 'BETWEEN');
  $db->groupBy("dayDate");
  $db->orderBy("dayDate", "asc");
  $wordsPerDay = $db->get(ANSWERS_TABLE_NAME, null, $cols);

  $cols = array("DATE(date) as dayDate", "COUNT(DISTINCT word) as wordOccurrence");
  $db->where('date', array($startDateString, $endDateString), 'BETWEEN');
  $db->groupBy("dayDate");
  $db->orderBy("date", "asc");
  $uniqueWordsPerDay = $db->get(ANSWERS_TABLE_NAME, null, $cols);

  $cols = array("email", "nick", "current_level", "current_question_id", 'timestamp');
  $currentPlayers = $db->get(TABLE_NAME, null, $cols);

  $output = array(
    "currentPlayers" => $currentPlayers,
    "endDate" => $endDateObj->format("Y-m-d"),
    "startDate" => $startDateObj->format("Y-m-d"),
    "uniqueWordsPerDay" => $uniqueWordsPerDay,
    "wordsPerDay" => $wordsPerDay,
    "wordsStat" => $wordsStat,
    "demoMode" => boolval(STATS_DEMO_MODE),
  );

  if (!is_null($deleteSuccess)) {
    $output["deleteSuccess"] = $deleteSuccess;
  }

  response($output);
} catch (Exception $e) {
  response($e->getMessage(), 500);
}
