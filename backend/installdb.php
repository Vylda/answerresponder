<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Create tables for Answer responder</title>
  <link rel="shortcut icon" href="../favicon.ico" />
  <style>
    body {
      font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
      font-size: 18px;
    }

    div {
      padding: 20px;
      line-height: 1.7;
    }

    code {
      display: block;
      padding: 10px;
      border: 2px solid gray;
      background-color: black;
      color: gainsboro;
      margin-top: 20px;
    }
  </style>
</head>

<body>
  <h1>ANSWER RESPONDER TABLES</h1>
  <div>
    <?php
    require_once './config.php';
    try {
      $mysqli = new mysqli(HOST, USER, PASSWORD, DB_NAME);
    } catch (Exception $e) {
      echo '❌ Base Database error: ',  $e->getMessage(), "\n";
      die();
    }

    try {
      if ($mysqli->connect_errno) {
        throw new ErrorException("Connect failed: " . $mysqli->connect_error . "\n",);
      }
      echo "Connect to database: ✅<br>";

      if (!$mysqli->query('SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";')) {
        throw new ErrorException("Settings failed");
      }
      echo "Basic settings: ✅<br>";

      if (!$mysqli->query('DROP TABLE IF EXISTS `' . TABLE_NAME . '`;')) {
        throw new ErrorException("Drop players table failed");
      }
      echo "Drop existing players table: ✅<br>";

      if (!$mysqli->query("CREATE TABLE IF NOT EXISTS `" . TABLE_NAME .
        "` (
  `email` varchar(100) COLLATE utf8_czech_ci NOT NULL COMMENT 'unique player\'s e-mail',
  `nick` text COLLATE utf8_czech_ci DEFAULT NULL COMMENT 'player\'s nick',
  `current_level` int(10) NOT NULL DEFAULT 0 COMMENT 'current game level',
  `current_question_id` int(10) NOT NULL DEFAULT 0 COMMENT 'current question index',
  `last_answer_id` int(10) DEFAULT NULL COMMENT 'index of last answer',
  `solved_questions` text COLLATE utf8_czech_ci DEFAULT NULL COMMENT 'already solved tasks',
  `recovery_password` text COLLATE utf8_czech_ci NOT NULL COMMENT 'recovery password (hash)',
  `token` text COLLATE utf8_czech_ci NOT NULL COMMENT 'control token',
  `timestamp` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'timestamp of last correct answer',
  UNIQUE KEY `emailprimary` (`email`) USING HASH
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='active players for Answer Responder';")) {
        throw new ErrorException("Players table creation failed");
      }
      echo "Create players table: ✅<br>";


      if (!$mysqli->query('DROP TABLE IF EXISTS `' . ANSWERS_TABLE_NAME . '`;')) {
        throw new ErrorException("Drop statistics table failed");
      }
      echo "Drop existing statistics table: ✅<br>";

      if (!$mysqli->query("CREATE TABLE IF NOT EXISTS `" . ANSWERS_TABLE_NAME . "` (
  `word_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'answer id',
  `word` varchar(300) COLLATE utf8_czech_ci NOT NULL COMMENT 'answer',
  `date` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'date of answer',
  PRIMARY KEY (`word_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='answer statistics';")) {
        throw new ErrorException("Statistics table creation failed");
      }
      echo "Create statistics table: ✅<br>";

      $mysqli->close();
      echo "All done: ✅<br>";
    } catch (Exception $e) {
      echo '❌ Error: ',  $e->getMessage(), "\n";
      echo "<code>" . $mysqli->error . "</code>";
      die();
    }
    ?>
  </div>
</body>

</html>
