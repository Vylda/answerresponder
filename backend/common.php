<?php

// https://github.com/PHPMailer/PHPMailer
require_once './mailer/Exception.php';
require_once './mailer/PHPMailer.php';
require_once './mailer/SMTP.php';

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * Replace %...% in string to replacement
 * @param String $string String to replace
 * @param String $replacement
 * @param String $patternName What I want replace
 * @return mixed
 */
function replaceString($string, $replacement, $patternName)
{
  switch ($patternName) {
    case "e-mail":
      $pattern = "/\%email\%/";
      break;
    case "level":
      $pattern = "/\%level\%/";
      break;
    case "next-level":
      $pattern = "/\%nextlevel\%/";
      break;
    case "answer":
      $pattern = "/\%answer\%/";
      break;
    case "nick":
      $pattern = "/\%nick\%/";
      break;
    default:
  }
  if (empty($pattern)) {
    return $string;
  }

  return preg_replace($pattern, $replacement, $string);
}

/**
 * Show messages as JSON and stop script with the given HTTP status code
 * @param Array $messageArray Arrays
 * @param int [$code = 200] HTTP status code
 * @return exit
 */
function response($messageArray, $code = 200)
{
  header('Content-type: application/json');
  http_response_code($code);
  echo json_encode($messageArray);
  die();
}

/**
 * Create 32-bit token
 * @return String
 */
function createToken()
{
  return bin2hex(random_bytes(32));
}

/**
 * Send new 32-bit token with status code 200
 * @return exit
 */
function sendToken()
{
  $newToken = ["token" => createToken()];
  response($newToken, 200);
}

/**
 * Get message from array: last or level based
 * @param Array $messageArray Array of messages
 * @param int $level
 * @return String
 */
function getCurrentMessage($messageArray, $level)
{
  $index = min(count($messageArray) - 1, $level);
  return $messageArray[$index];
}

/**
 * Configure mailer class and return instance
 * @param String $email Recipient¨s email
 * @return PHPMailer
 * @throws Exception
 */
function configureMailer($email)
{
  $mailer = new PHPMailer(true);
  $mailer->CharSet = 'UTF-8';
  $mailer->setFrom(OWNER_MAIL, OWNER_NAME);
  $mailer->addAddress($email);
  $mailer->XMailer = 'Answer Responder.cz';
  if (SEND_BCC) {
    $mailer->addBCC(OWNER_MAIL);
  }
  return $mailer;
}
/**
 * Send data for next level with HTTP status code 200
 * @param String $message Correct answetr message
 * @param int $level New level
 * @param String $email
 * @param String $nick
 * @return exit
 */
function sendGameLevelData($message, $level, $email, $nick)
{
  $correctMessageArray = explode("\n", $message);
  $levelMessage = getCurrentMessage(intl("NEXT_LEVEL_MESSAGES"), max($level - 1, 0));
  $levelMessage = replaceString($levelMessage, $email, "e-mail");
  $levelMessage = replaceString($levelMessage, $level, "level");
  $levelMessage = replaceString($levelMessage, $level + 1, "next-level");
  $levelMessage = replaceString($levelMessage, $nick, "nick");
  $levelMessageArray = explode("\n", $levelMessage);
  $correctMessages = array_merge($correctMessageArray, $levelMessageArray);
  $levelTitle = getCurrentMessage(intl("NEXT_LEVEL_TITLES"), max($level - 1, 0));
  $levelTitle = replaceString($levelTitle, $level, "level");
  $levelTitle = replaceString($levelTitle, $level + 1, "next-level");
  $levelTitle = replaceString($levelTitle, $nick, "nick");

  $result = [
    "level" => $level,
    "hints" => $correctMessages,
    "title" => $levelTitle,
    "completed" => false,
  ];

  response($result, 200);
}

/**
 * Send data for recovery mode with given HTTP status code
 * @param bool [$recoveryMode = true] show or hide recovery window
 * @param String|null [$recoveryError = null] Problem with recovery
 * @param String|null [$errorMessage = null] Rpoblem with backend
 * @param String|null [$successMessage = null] Success message
 * @param String|null [$token = null] New authentization token
 * @param int [$statusCode = 401]
 * @return exit
 */
function sendRecoveryMode(
  $recoveryMode = true,
  $recoveryError = null,
  $errorMessage = null,
  $successMessage = null,
  $token = null,
  $statusCode = 401
) {
  $data = [
    "recoveryMode" => $recoveryMode,
    "errorMessage" => $errorMessage,
    "recoveryError" => $recoveryError,
    "successMessage" => $successMessage,
    "token" => $token,
  ];
  response($data, $statusCode);
}

/**
 * Send backend error message with given HTTP status code
 * @param string [$errorMessage = ""]
 * @param int [$statusCode = 500]
 * @return exit
 */
function sendErrorMessage($errorMessage = "", $statusCode = 500)
{
  $data = [
    "errorMessage" => $errorMessage,
  ];
  response($data, $statusCode);
}

function sendNoTask($errorMessage = "", $nickname = "")
{
  $data = [
    "errorMessage" => $errorMessage,
    "nickname" => $nickname,
  ];
  response($data, 401);
}

/**
 * Send error message as answer error with given HTTP status code
 * @param string [$answer = ""]
 * @param string [$errorMessage = ""]
 * @param int [$statusCode = 500]
 * @return exit
 */
function sendAnswerErrorMessage($answer = "", $errorMessage = "", $statusCode = 500)
{
  $data = [
    "answer" => $answer,
    "errorMessage" => $errorMessage,
  ];
  response($data, $statusCode);
}

/**
 * Translations function
 * @param String $token code for translated string
 * @return String
 */
function intl($token)
{
  global $translations;
  global $langCode;
  if (empty($langCode)) {
    return $token;
  }

  if (!array_key_exists($langCode, $translations)) {
    $langCode = substr($langCode, 0, 2);

    if (!array_key_exists($langCode, $translations)) {
      $langCode = DEFAULT_LANGUAGE;
    }
  }

  if (!array_key_exists($token, $translations[$langCode])) {
    return $token;
  }

  return $translations[$langCode][$token];
}

/**
 * Translated tasks
 * @return Array
 */
function intlTasks()
{
  global $gameTasks;
  global $langCode;
  if (empty($langCode)) {
    return [];
  }

  if (!array_key_exists($langCode, $gameTasks)) {
    $langCode = substr($langCode, 0, 2);

    if (!array_key_exists($langCode, $gameTasks)) {
      $langCode = DEFAULT_LANGUAGE;
    }
  }

  return $gameTasks[$langCode];
}
