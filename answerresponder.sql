SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


DROP TABLE IF EXISTS `answerresponder`;
CREATE TABLE IF NOT EXISTS `answerresponder` (
  `email` varchar(100) COLLATE utf8_czech_ci NOT NULL COMMENT 'jedinečný e-mail hráče',
  `nick` text COLLATE utf8_czech_ci DEFAULT NULL,
  `current_level` int(10) NOT NULL DEFAULT 0 COMMENT 'aktuální úroveň hry',
  `current_question_id` int(10) NOT NULL DEFAULT 0 COMMENT 'aktuální číslo otázky',
  `last_answer_id` int(10) DEFAULT NULL COMMENT 'ID poslední odpovědi',
  `solved_questions` text COLLATE utf8_czech_ci DEFAULT NULL COMMENT 'již vyřešené úkoly',
  `recovery_password` text COLLATE utf8_czech_ci NOT NULL COMMENT 'heslo pro obnovení',
  `token` text COLLATE utf8_czech_ci NOT NULL COMMENT 'kontrolní token',
  `timestamp` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'timestamp poslední správné odpovědi',
  UNIQUE KEY `emailprimary` (`email`) USING HASH
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='aktuální hráči pro Answer Responder';

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
