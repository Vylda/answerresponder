module.exports = {
  extends: ['airbnb-typescript'],
  parserOptions: {
    project: './tsconfig.prod.json',
  },
  env: {
    browser: true,
    node: true
  },
  globals: {
    "PRODUCTION": true,
  },
  rules: {
    "no-console": ["error", { allow: ["warn", "error"] }]
  }
};
