import React, {
  FormEvent, useEffect, useRef, useState,
} from 'react';
import { IntlProvider, FormattedMessage } from 'react-intl';
import {
  Spinner, Toaster, FormGroup, Card, Elevation, InputGroup,
  Label, Button, Tooltip, Intent, Position, Alert,
} from '@blueprintjs/core';
import {
  langCode, messages, intl, getTranslations,
} from './langResolver';
import Footer from './components/Footer';
import DarkMode, { MODES } from './components/DarkMode';
import Translations from './components/Translations';
import DeleteSettings from './components/DeleteSettings';
import Users, { ICallbackProps } from './components/Users';
import Charts from './components/Charts';
import { postData, isEmpty } from './helpers';

interface IConfig {
  INSTANCE_NAME: string;
}
interface IAdditionalData {
  delete?: string;
  startDate?: string;
  endDate?: string;
}

/**
 * Global variables
 */
declare global {
  interface Window {
    gameConfig: IConfig;
  }
}

const toasterTimeout = 4500;

const Statistics = () => {
  const defUserDeleteData = { nick: '', email: '' };
  const [localeState, setLocaleState] = useState(langCode);
  const [messagesState, setMessagesState] = useState(messages);
  const [defaultMode, setdefaultMode] = useState(MODES.NONE);
  const [password, setPassword] = useState('');
  const [errorEmptyPass, setErrorEmptyPass] = useState(true);
  const [showPassword, setShowPassword] = useState(false);
  const [success, setSuccess] = useState(false);
  const [dataStats, setDataStats] = useState({});
  const [datums, setDatums] = useState(null);
  const [dataUsers, setDataUsers] = useState([]);
  const [dataDemoMode, setDataDemoMode] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isDeleteAlertOpen, setIsDeleteAlertOpen] = useState(false);
  const [deleteUserData, setDeleteUserData] = useState(defUserDeleteData);

  const { formatMessage } = intl;

  const toaster = useRef(null);
  const passwordBtn = useRef(null);

  const validate = (passwordToValidate: string) => {
    setErrorEmptyPass(!passwordToValidate);
  };

  const languageChangeHandler = (newLanguage: string) => {
    localStorage.setItem(`${window.gameConfig.INSTANCE_NAME}_defaultLanguage`, newLanguage);
    const newTranslation = getTranslations();

    setMessagesState(newTranslation.messages);
    setLocaleState(newTranslation.langCode);
  };

  const modeChangeHandler = (mode: MODES) => {
    localStorage.setItem(`${window.gameConfig.INSTANCE_NAME}_acDarkMode`, mode.toString());
  };

  const deleteSettingsHandler = () => {
    localStorage.removeItem(`${window.gameConfig.INSTANCE_NAME}_acDarkMode`);
    localStorage.removeItem(`${window.gameConfig.INSTANCE_NAME}_defaultLanguage`);
    const newTranslation = getTranslations();
    setLocaleState(newTranslation.langCode);
    setMessagesState(newTranslation.messages);
    setdefaultMode(MODES.NONE);
  };

  const handleChangePassword = (evt: FormEvent) => {
    const { target } = evt;
    const { value } = target as HTMLInputElement;
    setPassword(value);
  };

  const handleLockClick = () => {
    setShowPassword(!showPassword);
  };

  const fetchStatsHandler = (additionalData: IAdditionalData = {}) => {
    if (!password) {
      return;
    }

    setLoading(true);
    const params = new URLSearchParams();
    params.append('password', password.trim());
    if (!isEmpty(additionalData)) {
      const keys = Object.keys(additionalData);
      keys.forEach((key) => {
        const value = additionalData[key as keyof IAdditionalData];
        params.append(key.toString(), value);
      });
    }

    postData('stats', params).then((response: any) => {
      setLoading(false);
      setSuccess(true);
      sessionStorage.setItem(
        `${window.gameConfig.INSTANCE_NAME}_stats-password`,
        encodeURIComponent(password),
      );
      if (response.data) {
        setDataUsers(response.data.currentPlayers);
        const {
          endDate, startDate, uniqueWordsPerDay, wordsPerDay, wordsStat, deleteSuccess, demoMode,
        } = response.data;
        setDataStats({
          uniqueWordsPerDay, wordsPerDay, wordsStat,
        });
        setDatums({ endDate, startDate });
        setDataDemoMode(demoMode);
        if (typeof deleteSuccess === 'boolean') {
          toaster.current.show({
            message: intl.formatMessage({ id: `users.deleteResult.${deleteSuccess.toString()}` }),
            intent: deleteSuccess ? Intent.SUCCESS : Intent.DANGER,
            icon: deleteSuccess ? 'tick-circle' : 'error',
            toasterTimeout,
          });
        }
      } else {
        toaster.current.show({
          message: intl.formatMessage({ id: 'stats.error.nodata' }),
          intent: Intent.DANGER,
          icon: 'error',
          toasterTimeout,
        });
      }
    }).catch((error) => {
      // console.log(error.response);
      let messageId = 'general';
      if (
        'response' in error
        && 'data' in error.response
        && typeof error.response.data !== 'string'
        && 'error' in error.response.data
      ) {
        switch (error.response.data.error) {
          case 'PASSWORD MISSING':
            messageId = 'missing';
            break;
          case 'ACCESS DENIED':
            messageId = 'denied';
            break;
          case 'DELETE ERROR':
            messageId = 'delete';
            break;
          case 'BAD PASSWORD':
            messageId = 'bad';
            break;
          default:
        }
      }

      setLoading(false);
      setSuccess(false);

      toaster.current.show({
        message: intl.formatMessage({ id: `stats.error.${messageId}` }),
        intent: Intent.DANGER,
        icon: 'error',
        toasterTimeout,
      });
    });
  };

  const deleteUserHandler = (userData: ICallbackProps) => {
    setDeleteUserData(userData);
    setIsDeleteAlertOpen(true);
  };

  const deleteCancelHandler = () => {
    setDeleteUserData(defUserDeleteData);
    setIsDeleteAlertOpen(false);
  };

  const deleteConfirmHandler = () => {
    setIsDeleteAlertOpen(false);
    const { email } = deleteUserData;
    setDeleteUserData(defUserDeleteData);
    if (email) {
      const data: IAdditionalData = { delete: email };
      fetchStatsHandler(data);
    }
  };

  const refreshHandler = (data: { startDate: string, endDate: string }) => {
    fetchStatsHandler(data);
  };

  useEffect(() => {
    const modeNumber: number = parseInt(
      localStorage.getItem(`${window.gameConfig.INSTANCE_NAME}_acDarkMode`), 10,
    ) || 0;
    const mode = modeNumber as MODES;
    setdefaultMode(mode);

    document.title = intl.formatMessage({ id: 'stats.name' });
    setLoading(false);
    const passFromStorage = sessionStorage.getItem(
      `${window.gameConfig.INSTANCE_NAME}_stats-password`,
    );
    if (passFromStorage) {
      const encodedPassword = decodeURIComponent(passFromStorage);
      setPassword(encodedPassword);
      setTimeout(() => {
        if (passwordBtn.current) {
          passwordBtn.current.click();
        }
      }, 500);
    }
  }, []);

  useEffect(() => {
    const modeNumber: number = parseInt(
      localStorage.getItem(`${window.gameConfig.INSTANCE_NAME}_acDarkMode`), 10,
    ) || 0;
    const mode = modeNumber as MODES;
    setdefaultMode(mode);
  }, [localeState]);

  useEffect(() => {
    validate(password);
  }, [password]);

  const lockButton = (
    <Tooltip content={intl.formatMessage(
      {
        id: 'recovery.showHidePassword',
      },
      {
        showHide: intl.formatMessage({
          id: showPassword ? 'recovery.hide' : 'recovery.show',
        }),
      },
    )}
    >
      <Button
        icon={showPassword ? 'unlock' : 'lock'}
        intent={Intent.WARNING}
        minimal
        onClick={handleLockClick}
      />
    </Tooltip>
  );

  return (
    <IntlProvider key={localeState} locale={localeState} messages={messagesState}>
      {loading
        ? (
          <div className="loader">
            <Spinner intent="primary" size={Spinner.SIZE_LARGE} />
          </div>
        )
        : (
          <>
            <Card elevation={3}>
              <h1>
                <FormattedMessage id="stats.name" />
                {dataDemoMode && (
                  <>
                    {' – '}
                    <FormattedMessage id="stats.demoMode" />
                  </>
                )}
              </h1>
              {dataDemoMode && (
                <p className="demo-mode-info">
                  <FormattedMessage id="stats.demoModeInfo" />
                </p>
              )}
              {!success && (
                <form>
                  <FormGroup>
                    <Label htmlFor="input-password-stats">
                      <FormattedMessage id="password.label" />
                    </Label>
                    <InputGroup
                      leftIcon="key"
                      placeholder={intl.formatMessage({ id: 'password.placeholder' })}
                      onChange={handleChangePassword}
                      value={password}
                      id="input-password-stats"
                      type={showPassword ? 'text' : 'password'}
                      rightElement={lockButton}
                      required
                    />
                    {errorEmptyPass && (
                      <div className="error">
                        <FormattedMessage id="password.empty" />
                      </div>
                    )}

                    <div className="button-line">
                      <Button
                        intent={Intent.SUCCESS}
                        disabled={errorEmptyPass}
                        onClick={() => fetchStatsHandler()}
                        elementRef={passwordBtn}
                        type="submit"
                      >
                        <FormattedMessage id="stats.send" />
                      </Button>
                    </div>
                  </FormGroup>
                </form>
              )}
              {success && (
                <>
                  <Users users={dataUsers} onDelete={deleteUserHandler} />
                  <Charts data={dataStats} datums={datums} onRefresh={refreshHandler} />
                </>
              )}
              <Footer />
            </Card>
            <Card elevation={Elevation.THREE} className="app-settings">
              <DarkMode switchTitleKey="darkMode" defaultMode={defaultMode} onChange={modeChangeHandler} />
              <Translations
                defaultLanguage={localeState}
                onChange={languageChangeHandler}
                buttonTitleKey="setTranslation"
              />
              <DeleteSettings onClick={deleteSettingsHandler} />
            </Card>
          </>
        )}
      <Alert
        cancelButtonText={formatMessage({ id: 'users.cancel' })}
        confirmButtonText={formatMessage({ id: 'users.confirm' })}
        icon="trash"
        intent={Intent.DANGER}
        isOpen={isDeleteAlertOpen}
        onCancel={deleteCancelHandler}
        onConfirm={deleteConfirmHandler}
      >
        <p>
          <FormattedMessage id="users.deleteAlert" values={{ nick: deleteUserData.nick }} />
        </p>
      </Alert>
      <Toaster
        ref={toaster}
        position={Position.TOP}
        className="stats-toaster"
      />
    </IntlProvider>
  );
};

export default Statistics;
