import { createIntl, createIntlCache } from 'react-intl';

const tableize = require('tableize-object');

interface IConfig {
  INSTANCE_NAME: string;
  DEFAULT_LANGUAGE: string;
}

declare namespace window {
  const gameConfig: IConfig;
  const translations: any;
}

type TMessages = {
  [key: string]: string;
};

/**
 * Test, if object is empty
 * @param obj
 */
const emptyObject = (obj: any) => {
  const keysLength = Object.keys(obj).length;
  return keysLength === 0 && obj.constructor === Object;
};

/**
 * Get translation object
 * @returns object {langCode, translations, intl function}
 */
export const getTranslations = (): {
  langCode: string;
  messages: TMessages;
  intl: any;
} => {
  let langCode: any = localStorage.getItem(`${window.gameConfig.INSTANCE_NAME}_defaultLanguage`)
    || navigator.language.toLowerCase();

  function getProperty<T, K extends keyof T>(o: T, property: K): T[K] {
    return o[property];
  }

  let messages: TMessages = tableize(getProperty(window.translations, langCode));

  if (emptyObject(messages)) {
    langCode = langCode.slice(0, 2);
    messages = tableize(getProperty(window.translations, langCode));

    if (emptyObject(messages)) {
      langCode = window.gameConfig.DEFAULT_LANGUAGE;
      messages = tableize(window.translations[langCode]);
    }
  }

  const cache = createIntlCache();
  const intl = createIntl({
    locale: langCode.toString(),
    messages,
  }, cache);

  return { langCode: langCode.toString(), messages, intl };
};

export const { langCode, messages, intl } = getTranslations();

export const languageMutations = (): string[] => Object.keys(window.translations);
