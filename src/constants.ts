export const CHANGE_TASK: string = 'CHANGE_TASK';
export const CHANGE_EMAIL: string = 'CHANGE_EMAIL';
export const CHANGE_ANSWER: string = 'CHANGE_ANSWER';
export const CHANGE_NICK: string = 'CHANGE_NICK';
export const SET_ERRORS: string = 'SET_ERRORS';
export const SET_BE_ERROR_MESSAGE: string = 'SET_BE_ERROR_MESSAGE';
export const SET_EMAIL_ERRORS: string = 'SET_EMAIL_ERRORS';
export const SET_LOADING: string = 'SET_LOADING';
export const SET_RECOVERY_MODE: string = 'SET_RECOVERY_MODE';
export const SET_IS_BAD_ANSWER: string = 'SET_IS_BAD_ANSWER';

export const recoveryModes = {
  DELETE: 'delete',
  RECOVER: 'recover',
};

export const PALETTE = [
  '#8dd3c7', '#bebada', '#fb8072', '#80b1d3', '#fdb462', '#b3de69', '#fccde5', '#1a9850',
  '#d9d9d9', '#bc80bd', '#ccebc5', '#51574a', '#447c69', '#74c493', '#8e8c6d', '#e4bf80',
  '#e9d78e', '#e2975d', '#f19670', '#e16552', '#c94a53', '#be5168', '#a34974', '#993767',
  '#65387d', '#4e2472', '#9163b6', '#e279a3', '#e0598b', '#7c9fb0', '#5698c4', '#9abf88',
  '#1a1334', '#26294a', '#01545a', '#017351', '#03c383', '#aad962', '#fbbf45', '#ef6a32',
  '#ed0345', '#a12a5e', '#710162', '#110141', '#543005', '#8c510a', '#bf812d', '#dfc27d',
  '#f6e8c3', '#f5f5f5', '#c7eae5', '#80cdc1', '#35978f', '#01665e', '#003c30', '#a50026',
  '#d73027', '#f46d43', '#fdae61', '#fee08b', '#ffffbf', '#d9ef8b', '#a6d96a', '#66bd63',
  '#006837', '#ffffb3',
];
export const minPieValueDeg = 12; // minimal degree value for piechart
