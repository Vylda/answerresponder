import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import Statistics from './Statistics';
import '../style/style.less';

ReactDOM.render(
  <Provider store={store}>
    <Statistics />
  </Provider>,
  document.getElementById('app'),
);
