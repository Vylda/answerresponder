import React, {
  FormEvent, useEffect, useRef, useState,
} from 'react';
import { IntlProvider, FormattedMessage } from 'react-intl';
import {
  Spinner, Toaster, FormGroup, Card, Elevation, InputGroup,
  Label, Button, Tooltip, Intent, Position,
} from '@blueprintjs/core';
import {
  langCode, messages, intl, getTranslations,
} from './langResolver';
import Footer from './components/Footer';
import DarkMode, { MODES } from './components/DarkMode';
import Translations from './components/Translations';
import DeleteSettings from './components/DeleteSettings';
import { postData } from './helpers';

interface IConfig {
  INSTANCE_NAME: string;
}

/**
 * Global variables
 */
declare global {
  interface Window {
    gameConfig: IConfig;
  }
}

const PasswordGenerator = () => {
  const [localeState, setLocaleState] = useState(langCode);
  const [messagesState, setMessagesState] = useState(messages);
  const [defaultMode, setdefaultMode] = useState(MODES.NONE);
  const [password, setPassword] = useState('');
  const [errorEmptyPass, setErrorEmptyPass] = useState(true);
  const [showPassword, setShowPassword] = useState(false);
  const [hash, setHash] = useState('');
  const [hashError, setHashError] = useState(false);
  const [loading, setLoading] = useState(true);

  const toaster = useRef(null);

  const validate = (passwordToValidate: string) => {
    setErrorEmptyPass(!passwordToValidate);
  };

  const languageChangeHandler = (newLanguage: string) => {
    localStorage.setItem(`${window.gameConfig.INSTANCE_NAME}_defaultLanguage`, newLanguage);
    const newTranslation = getTranslations();

    setMessagesState(newTranslation.messages);
    setLocaleState(newTranslation.langCode);
  };

  const modeChangeHandler = (mode: MODES) => {
    localStorage.setItem(`${window.gameConfig.INSTANCE_NAME}_acDarkMode`, mode.toString());
  };

  const deleteSettingsHandler = () => {
    localStorage.removeItem(`${window.gameConfig.INSTANCE_NAME}_acDarkMode`);
    localStorage.removeItem(`${window.gameConfig.INSTANCE_NAME}_defaultLanguage`);
    const newTranslation = getTranslations();
    setLocaleState(newTranslation.langCode);
    setMessagesState(newTranslation.messages);
    setdefaultMode(MODES.NONE);
  };

  const handleChangePassword = (evt: FormEvent) => {
    const { target } = evt;
    const { value } = target as HTMLInputElement;
    setPassword(value);
    setHash('');
    setHashError(false);
  };

  const handleLockClick = () => {
    setShowPassword(!showPassword);
  };

  const handleCopyClick = () => {
    const timeout = 3000;
    try {
      navigator.clipboard.writeText(hash).then(() => {
        toaster.current.show({
          message: intl.formatMessage({ id: 'password.copySuccess' }),
          intent: Intent.SUCCESS,
          icon: 'tick',
          timeout,
        });
      }, () => {
        toaster.current.show({
          message: intl.formatMessage({ id: 'password.copyError' }),
          intent: Intent.DANGER,
          icon: 'error',
          timeout,
        });
      });
    } catch (err) {
      toaster.current.show({
        message: intl.formatMessage({ id: 'password.copyError' }),
        intent: Intent.DANGER,
        icon: 'error',
        timeout,
      });
    }
  };

  const getPasswordHashHandler = () => {
    if (!password) {
      return;
    }

    setLoading(true);
    const params = new URLSearchParams();
    params.append('password', password.trim());

    postData('passwordgenerator', params).then((response: any) => {
      const { passwordHash } = response.data;
      setLoading(false);
      setHash(passwordHash);
      setHashError(!passwordHash);
    }).catch(() => {
      setLoading(false);
      setHashError(true);
    });
  };

  useEffect(() => {
    const modeNumber: number = parseInt(
      localStorage.getItem(`${window.gameConfig.INSTANCE_NAME}_acDarkMode`), 10,
    ) || 0;
    const mode = modeNumber as MODES;
    setdefaultMode(mode);

    document.title = intl.formatMessage({ id: 'password.name' });
    setLoading(false);
  }, []);

  useEffect(() => {
    const modeNumber: number = parseInt(
      localStorage.getItem(`${window.gameConfig.INSTANCE_NAME}_acDarkMode`), 10,
    ) || 0;
    const mode = modeNumber as MODES;
    setdefaultMode(mode);
  }, [localeState]);

  useEffect(() => {
    validate(password);
  }, [password]);

  const lockButton = (
    <Tooltip content={intl.formatMessage(
      {
        id: 'recovery.showHidePassword',
      },
      {
        showHide: intl.formatMessage({
          id: showPassword ? 'recovery.hide' : 'recovery.show',
        }),
      },
    )}
    >
      <Button
        icon={showPassword ? 'unlock' : 'lock'}
        intent={Intent.WARNING}
        minimal
        onClick={handleLockClick}
      />
    </Tooltip>
  );

  const copyButton = (
    <Tooltip content={intl.formatMessage(
      {
        id: 'password.copy',
      },
    )}
    >
      <Button
        icon="clipboard"
        intent={Intent.PRIMARY}
        minimal
        disabled={!hash}
        onClick={handleCopyClick}
      />
    </Tooltip>
  );

  return (
    <IntlProvider key={localeState} locale={localeState} messages={messagesState}>
      {loading
        ? (
          <div className="loader">
            <Spinner intent="primary" size={Spinner.SIZE_LARGE} />
          </div>
        )
        : (
          <>
            <Card elevation={3}>
              <h1>
                <FormattedMessage id="password.name" />
              </h1>
              <FormGroup>
                <Label htmlFor="input-password">
                  <FormattedMessage id="password.label" />
                </Label>
                <InputGroup
                  leftIcon="key"
                  placeholder={intl.formatMessage({ id: 'password.placeholder' })}
                  onChange={handleChangePassword}
                  value={password}
                  id="input-password"
                  type={showPassword ? 'text' : 'password'}
                  rightElement={lockButton}
                  required
                />
                {errorEmptyPass && (
                  <div className="error">
                    <FormattedMessage id="password.empty" />
                  </div>
                )}

                <Label htmlFor="input-hash">
                  <FormattedMessage id="password.hashLabel" />
                </Label>
                <InputGroup
                  leftIcon="archive"
                  placeholder={intl.formatMessage({ id: 'password.hashPlaceholder' })}
                  onChange={handleChangePassword}
                  value={hash}
                  id="input-hash"
                  type="text"
                  readOnly
                  rightElement={copyButton}
                />
                {hashError && (
                  <div className="error">
                    <FormattedMessage id="password.hashError" />
                  </div>
                )}
                <div className="button-line">
                  <Button
                    intent={Intent.SUCCESS}
                    disabled={errorEmptyPass}
                    onClick={getPasswordHashHandler}
                  >
                    <FormattedMessage id="password.send" />
                  </Button>
                </div>
              </FormGroup>
              <Footer />
            </Card>
            <Card elevation={Elevation.THREE} className="app-settings">
              <DarkMode switchTitleKey="darkMode" defaultMode={defaultMode} onChange={modeChangeHandler} />
              <Translations
                defaultLanguage={localeState}
                onChange={languageChangeHandler}
                buttonTitleKey="setTranslation"
              />
              <DeleteSettings onClick={deleteSettingsHandler} />
            </Card>
            <Toaster
              ref={toaster}
              position={Position.TOP}
              className="password-toaster"
            />
          </>
        )}
    </IntlProvider>
  );
};

export default PasswordGenerator;
