import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import PasswordGenerator from './PasswordGenerator';
import '../style/style.less';

ReactDOM.render(
  <Provider store={store}>
    <PasswordGenerator />
  </Provider>,
  document.getElementById('app'),
);
