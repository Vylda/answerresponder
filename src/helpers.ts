import axios from 'axios';
import store from './store';
import {
  SET_BE_ERROR_MESSAGE, CHANGE_TASK, CHANGE_ANSWER, SET_LOADING,
  SET_RECOVERY_MODE, SET_IS_BAD_ANSWER, CHANGE_NICK, recoveryModes,
} from './constants';
import { IRecovery } from './reducers/recoveryMode';
import { intl, langCode } from './langResolver';

interface IConfig {
  API_URL: string;
  INSTANCE_NAME: string;
}

declare namespace window {
  const gameConfig: IConfig;
}

const ls: Storage = localStorage;
const backend: string = window.gameConfig.API_URL;

/**
 * Delete all app storage items
 */
const deleteLocalStorage = () => {
  ls.removeItem(`${window.gameConfig.INSTANCE_NAME}_email`);
  ls.removeItem(`${window.gameConfig.INSTANCE_NAME}_token`);
};

/**
 * Get email from local storage
 */
export const getStoredEmail = (): string => {
  const email = ls.getItem(`${window.gameConfig.INSTANCE_NAME}_email`);
  return email ? decodeURIComponent(email) : '';
};

/**
 * Get token from local storage
 */
export const getStoredToken = (): string => {
  const token = ls.getItem(`${window.gameConfig.INSTANCE_NAME}_token`);
  return token || '';
};

/**
 * Save email to loal storage
 * @param email
 */
export const storeEmail = (email: string) => {
  ls.setItem(`${window.gameConfig.INSTANCE_NAME}_email`, encodeURIComponent(email));
};

/**
 * Save token to local storage
 * @param token
 */
export const storeToken = (token: string) => {
  ls.setItem(`${window.gameConfig.INSTANCE_NAME}_token`, token);
};

/**
 * Dispatch error from API to app store
 * @param errorMessage
 */
const dispatchBeError = (errorMessage: string) => {
  store.dispatch({
    type: SET_BE_ERROR_MESSAGE,
    errorMessage,
  });
};

/**
 * Dispatch loading app to app store
 * @param loading
 */
const dispatchLoading = (loading: boolean) => {
  store.dispatch({
    type: SET_LOADING,
    loading,
  });
};

/**
 * Dispatch bad answer info to app store
 * @param isBadAnswer
 */
const dispatchIsBadAnwer = (isBadAnswer: boolean) => {
  store.dispatch({
    type: SET_IS_BAD_ANSWER,
    isBadAnswer,
  });
};

/**
 * Disptch recovery data to app store
 * @param recovery
 */
const dispatchRecoveryMode = (recovery: IRecovery) => {
  store.dispatch({
    type: SET_RECOVERY_MODE,
    recovery,
  });
};

/**
 * Show error in info window
 * @param responseObj
 */
const showError = (responseObj: any) => {
  if (!responseObj) {
    dispatchBeError(intl.formatMessage({ id: 'error.noBackend' }));
    return;
  }

  const { response } = responseObj;
  const { errorMessage } = response ? response.data : { errorMessage: '' };

  dispatchBeError(errorMessage || intl.formatMessage({ id: 'error.backend' }));
};

/**
 * Shod bad answer message in info window
 * @param errorMessage
 */
const showBadAnswer = (errorMessage: string): void => {
  dispatchIsBadAnwer(true);
  dispatchBeError(errorMessage || intl.formatMessage({ id: 'error.backend' }));
};

/**
 * Show recovery modal window or error
 * @param responseObj
 */
const recoveryModeOrError = (responseObj: any) => {
  if (responseObj
    && 'response' in responseObj
    && responseObj.response
    && 'data' in responseObj.response
    && typeof responseObj.response.data === 'object') {
    if ('recoveryMode' in responseObj.response.data) {
      const {
        recoveryMode, recoveryError, successMessage,
      } = responseObj.response.data;
      dispatchRecoveryMode({
        recoveryMode,
        recoveryError: recoveryError || '',
        successMessage: successMessage || '',
      });
    } else if (
      'answer' in responseObj.response.data
      && 'errorMessage' in responseObj.response.data
      && responseObj.response.status === 404
    ) {
      const { data } = responseObj.response;
      showBadAnswer(data.errorMessage);
    } else {
      if ('nickname' in responseObj.response.data) {
        store.dispatch({
          type: CHANGE_NICK,
          nick: responseObj.response.data.nickname,
        });
      }
      showError(responseObj);
    }
  } else {
    showError(responseObj);
  }
};

/**
 * Post data to API
 * @param endpoint
 * @param params
 */
export const postData = (endpoint: string, params: any) => {
  const url = `${backend}/${endpoint}.php`;

  const config = {
    headers: {
      'X-Send-By': 'ar-app',
      'X-lang': langCode,
    },
  };
  return axios.post(url, params, config);
};

/**
 * Main method on new app load
 */
export const start = () => {
  dispatchLoading(true);

  const params = new URLSearchParams();
  params.append('email', getStoredEmail().trim());
  params.append('token', getStoredToken().trim());

  postData('start', params).then((response) => {
    const {
      level, completed, hints, title, token,
    } = response.data;
    if (hints && title) {
      store.dispatch({
        type: CHANGE_TASK,
        level,
        completed,
        hints,
        title,
      });
    }

    if (token) {
      storeToken(token);
    }

    dispatchLoading(false);
  }).catch((responseObj) => {
    recoveryModeOrError(responseObj);

    dispatchLoading(false);
  });
};

/**
 * Resolve data from recovery endpoint
 * @param data
 * @param mode
 */
const resolveRecoveryMode = (data: any, mode: string = ''): void => {
  const {
    errorMessage, recoveryError, recoveryMode, successMessage, token,
  } = data;

  if (errorMessage) {
    dispatchBeError(errorMessage);
  }
  if (token) {
    storeToken(token);
  }

  dispatchRecoveryMode({
    recoveryError,
    recoveryMode,
    successMessage: successMessage || '',
  });

  if (successMessage && mode === recoveryModes.RECOVER) {
    start();
  }
};

/**
 * Completly delete storage
 * @param email
 */
export const deleteStorage = (email: string) => {
  dispatchLoading(true);
  dispatchBeError('');
  dispatchIsBadAnwer(false);

  const params = new URLSearchParams();
  params.append('email', email);
  params.append('token', getStoredToken());

  postData('delete', params).then((data) => {
    resolveRecoveryMode(data.data);
    deleteLocalStorage();
  }).catch((responseObj) => {
    showError(responseObj);
  });

  dispatchLoading(false);
};

/**
 * Send answer to API
 */
export const sendAnswer = () => {
  dispatchLoading(true);
  dispatchBeError('');
  dispatchIsBadAnwer(false);

  const state = store.getState();

  if (state.errors) {
    return;
  }

  const { email, answer, nick } = state.form;
  storeEmail(email);

  const params = new URLSearchParams();
  params.append('email', email.trim());
  params.append('answer', answer.trim());
  params.append('nick', nick.trim());
  params.append('token', getStoredToken());

  postData('answer', params).then((response) => {
    const {
      level, completed, hints, title,
    } = response.data;
    store.dispatch({
      type: CHANGE_TASK,
      level,
      completed,
      hints,
      title,
    });

    store.dispatch({
      type: CHANGE_ANSWER,
      answer: '',
    });

    if (completed) {
      deleteLocalStorage();
    }

    dispatchLoading(false);
  }).catch((responseObj) => {
    recoveryModeOrError(responseObj);

    dispatchLoading(false);
  });
};

/**
 * Reset or recover game
 * @param password
 * @param mode
 */
export const resetGame = async (password: string, mode: string) => {
  dispatchLoading(true);
  const params = new URLSearchParams();
  params.append('email', getStoredEmail().trim());
  params.append('password', password.trim());
  params.append('mode', mode);

  try {
    const response = await postData('resetGame', params);
    resolveRecoveryMode(response.data, mode);
  } catch (error) {
    const errorMsg = intl.formatMessage({
      id: `error.recovery.${mode === recoveryModes.RECOVER ? 'recover' : 'delete'}`,
    });

    dispatchRecoveryMode({
      recoveryMode: true,
      recoveryError: errorMsg,
      successMessage: '',
    });
  }
  dispatchLoading(false);
};

/**
 * Test if object is empty
 * @param obj Tested object
 */
export const isEmpty = (obj: any) => {
  const keys = Object.keys(obj);
  return !keys.some((key) => Object.prototype.hasOwnProperty.call(obj, key));
};
