const defaultLang = window.gameConfig.DEFAULT_LANGUAGE;

const LocaleUtils = () => {
  const formatDay = (date: Date, locale: string = defaultLang): string => (
    date.toLocaleDateString(locale)
  );

  const formatMonthTitle = (date: Date, locale: string = defaultLang): string => {
    const options = { month: 'long', year: 'numeric' };
    return new Intl.DateTimeFormat(locale, options).format(new Date());
  };

  const formatWeekdayShort = (weekDay: number, locale: string = defaultLang): string => {
    const date = new Date();
    while (date.getDay() !== weekDay) {
      date.setDate(date.getDate() + 1);
    }
    const options = { weekday: 'short' };
    return new Intl.DateTimeFormat(locale, options).format(date);
  };

  const formatWeekdayLong = (weekDay: number, locale: string = defaultLang): string => {
    const date = new Date();
    while (date.getDay() !== weekDay) {
      date.setDate(date.getDate() + 1);
    }
    const options = { weekday: 'long' };
    return new Intl.DateTimeFormat(locale, options).format(date);
  };

  type TWeekStIn = {
    [key: string]: number;
  };

  const getFirstDayOfWeek = (locale: string = defaultLang): number => {
    const weeksStarts = window.gameConfig.WEEK_STARTS_IN as TWeekStIn;
    return weeksStarts[locale] || 0;
  };

  const getMonths = (locale: string = defaultLang): [
    string, string, string, string, string, string,
    string, string, string, string, string, string,
  ] => {
    const options = { month: 'long' };
    const date = new Date();
    const months: [
      string, string, string, string, string, string,
      string, string, string, string, string, string,
    ] = ['', '', '', '', '', '', '', '', '', '', '', ''];
    for (let month = 0; month < 12; month += 1) {
      date.setMonth(month);
      months[month] = (new Intl.DateTimeFormat(locale, options).format(date));
    }

    return months;
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const formatDate = (date: Date, format: string | string[] = '', locale: string = defaultLang): string => (
    new Intl.DateTimeFormat(locale).format(date)
  );

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const parseDate = (dateString: string, format: string = '', locale: string = defaultLang): Date => {
    const date = new Date();
    date.setDate(30);
    date.setMonth(11);
    date.setFullYear(3456);
    const localeString = date.toLocaleDateString(locale);

    const patternDate = localeString.replace(/\//g, '\\/')
      .replace(/-/g, '-')
      .replace(/\./g, '\\.')
      .replace('30', '([0-3]?[0-9])')
      .replace('12', '[01]?[0-9]')
      .replace('3456', '\\d{1,}')
      .replace(/\s/g, '\\s*');
    const patternMonth = localeString.replace(/\//g, '\\/')
      .replace(/-/g, '-')
      .replace(/\./g, '\\.')
      .replace('30', '[0-3]?[0-9]')
      .replace('12', '([01]?[0-9])')
      .replace('3456', '\\d{1,}')
      .replace(/\s/g, '\\s*');
    const patternYear = localeString.replace(/\//g, '\\/')
      .replace(/-/g, '-')
      .replace(/\./g, '\\.')
      .replace('30', '[0-3]?[0-9]')
      .replace('12', '[01]?[0-9]')
      .replace('3456', '(\\d{1,})')
      .replace(/\s/g, '\\s*');
    const regexDate = new RegExp(`^${patternDate}$`);
    const regexMonth = new RegExp(`^${patternMonth}$`);
    const regexYear = new RegExp(`^${patternYear}$`);

    const compressedDateString = dateString.replace(/\s/g, '');

    const matchesDate = compressedDateString.match(regexDate);
    const matchesMonth = compressedDateString.match(regexMonth);
    const matchesYear = compressedDateString.match(regexYear);

    if (!matchesDate || !matchesMonth || !matchesYear) {
      return new Date(Date.parse('error'));
    }
    const day = parseInt(matchesDate[1], 10);
    const month = parseInt(matchesMonth[1], 10);
    const year = parseInt(matchesYear[1], 10);

    return new Date(year, month - 1, day);
  };

  return {
    formatDay,
    formatMonthTitle,
    formatWeekdayLong,
    formatWeekdayShort,
    getFirstDayOfWeek,
    getMonths,
    formatDate,
    parseDate,
  };
};

export default LocaleUtils();
