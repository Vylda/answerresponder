import {
  CHANGE_EMAIL, CHANGE_ANSWER, SET_ERRORS, CHANGE_NICK,
  SET_BE_ERROR_MESSAGE, SET_EMAIL_ERRORS, CHANGE_TASK,
  SET_RECOVERY_MODE,
} from './constants';
import { IRecovery } from './reducers/recoveryMode';

export const setEmail = (email: string) => ({
  type: CHANGE_EMAIL,
  email,
});

export const setAnswer = (answer: string) => ({
  type: CHANGE_ANSWER,
  answer,
});

export const setNick = (nick: string) => ({
  type: CHANGE_NICK,
  nick,
});

export const setErrors = (errors: boolean) => ({
  type: SET_ERRORS,
  errors,
});

export const setEmailError = (error: boolean) => ({
  type: SET_EMAIL_ERRORS,
  error,
});

export const setRecovery = (recovery: IRecovery) => ({
  type: SET_RECOVERY_MODE,
  recovery,
});

export const setBeErrorMessage = (beErrorMessage: string) => ({
  type: SET_BE_ERROR_MESSAGE,
  beErrorMessage,
});

interface ITask {
  level: number;
  completed: boolean;
  hints: string[];
  title: string;
}
export const changeTask = (task: ITask) => {
  const {
    level, completed, hints, title,
  } = task;
  return {
    type: CHANGE_TASK,
    level,
    completed,
    hints,
    title,
  };
};
