import React, {
  Fragment, useEffect, useState,
} from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { Card, Elevation } from '@blueprintjs/core';
import { getTask, getBeErrorMessage, getIsBadAnswer } from '../selectors';

/**
 * Top infos
 */
const Question = ({
  task,
  beErrorMessage,
  isBadAnswer,
}: {
  task: any;
  beErrorMessage: string;
  isBadAnswer: boolean;
}) => {
  const [header, setHeader] = useState('');
  const [body, setBody] = useState([]);
  const [gameLevel, setGameLevel] = useState(0);
  const [cardClass, setCardClass] = useState('question');

  useEffect(() => {
    const {
      hints, title, level, completed,
    } = task;
    setGameLevel(level);
    setHeader(title);
    setBody([...hints]);
    if (level > 0) {
      setCardClass(completed ? 'question completed' : 'question correct');
    } else {
      setCardClass('question');
    }
  }, [task]);

  /**
   * Create text body
   * @param hints Array of strings
   */
  const createBody = (hints: Array<string>) => {
    const texts = hints.map((text, index) => {
      const replacedText = text.replace(/%level%/g, (gameLevel + 1).toString(10));
      const key = `text-${index}-${gameLevel}`;
      if (index === hints.length - 1) {
        return <Fragment key="text-last">{replacedText}</Fragment>;
      }
      return (
        <Fragment key={key}>
          {replacedText}
          <br />
        </Fragment>
      );
    });
    return texts;
  };

  if (beErrorMessage) {
    return (
      <Card elevation={Elevation.TWO} className="question error">
        <h2>
          {isBadAnswer
            ? <FormattedMessage id="task.badAnswerHeader" />
            : <FormattedMessage id="error.backendHeader" />}
        </h2>
        <p>
          {beErrorMessage}
        </p>
      </Card>
    );
  }

  return (
    <Card elevation={Elevation.TWO} className={cardClass}>
      <h2>{header}</h2>
      <p>{createBody(body)}</p>
    </Card>
  );
};

const mapStateToProps = (state: any) => ({
  task: getTask(state),
  beErrorMessage: getBeErrorMessage(state),
  isBadAnswer: getIsBadAnswer(state),
});

export default connect(mapStateToProps)(Question);
