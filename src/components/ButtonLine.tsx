import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { Button } from '@blueprintjs/core';
import {
  setEmail, setAnswer, setNick, changeTask,
} from '../reduxActions';
import {
  getErrors, getEmailError, getEmail, getLevel,
} from '../selectors';
import {
  deleteStorage, sendAnswer, start, storeEmail,
} from '../helpers';
import { defaultState as defaultTask } from '../reducers/task';

/**
 * Buttons below form
 */
const ButtonLine = ({
  errors,
  emailError,
  email,
  level,
  setEmailFunc,
  setAnswerFunc,
  setNickFunc,
  changeTaskFunc,
}: {
  errors: boolean;
  emailError: boolean;
  email: string;
  level: number;
  setEmailFunc: Function;
  setAnswerFunc: Function;
  setNickFunc: Function;
  changeTaskFunc: Function;
}) => {
  const [loading, setLoading] = useState(false);
  /**
   * Send answer
   */
  const handleSend = () => {
    if (!errors) {
      setLoading(true);
    }
  };

  useEffect(() => {
    if (loading) {
      sendAnswer();
    }
  }, [loading]);

  /**
   * Reset game
   */
  const handleResetGame = () => {
    deleteStorage(email);
    setEmailFunc('');
    setAnswerFunc('');
    setNickFunc('');
    changeTaskFunc(defaultTask);
    start();
  };

  /**
   * Reload game
   */
  const handleReloadGame = () => {
    storeEmail(email);
    start();
  };

  return (
    <div className="button-line">
      <Button
        icon="send-to"
        intent="success"
        large
        onClick={handleSend}
        disabled={errors || loading}
        type="submit"
      >
        <FormattedMessage id="button.sendAnswer" />
      </Button>
      {level !== 0 && !emailError && (
        <Button
          icon="refresh"
          intent="danger"
          large
          onClick={handleResetGame}
          disabled={emailError}
        >
          <FormattedMessage id="button.startFromZero" />
        </Button>
      )}
      {level === 0 && (
        <Button
          icon="refresh"
          intent="warning"
          large
          onClick={handleReloadGame}
          disabled={emailError}
        >
          <FormattedMessage id="button.loadGame" />
        </Button>
      )}
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  errors: getErrors(state),
  emailError: getEmailError(state),
  email: getEmail(state),
  level: getLevel(state),
});

const mapDispatchToProps = {
  setEmailFunc: setEmail,
  setAnswerFunc: setAnswer,
  setNickFunc: setNick,
  changeTaskFunc: changeTask,
};

export default connect(mapStateToProps, mapDispatchToProps)(ButtonLine);
