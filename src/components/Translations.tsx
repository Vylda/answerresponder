import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { MenuItem, Button } from '@blueprintjs/core';
import { Select, ItemRenderer } from '@blueprintjs/select';
import ISO6391 from 'iso-639-1';
import { languageMutations } from '../langResolver';

type ITranslationProps = {
  defaultLanguage: string;
  onChange?: Function;
  buttonTitleKey?: string;
};

const Translations = ({ defaultLanguage, onChange, buttonTitleKey }: ITranslationProps) => {
  const [activeElement, setActiveElement] = useState(null);
  const [options] = useState(languageMutations);
  const { formatMessage } = useIntl();

  const handleActiveItemChange = (
    activeItem: string | null,
  ) => {
    setActiveElement(activeItem);
    if (onChange) {
      onChange(activeItem.toString());
    }
  };

  const renderItem: ItemRenderer<string> = (item: string, { handleClick, modifiers }) => {
    const active = modifiers.active || (!activeElement && item === defaultLanguage);
    const element = (
      <MenuItem
        active={active}
        key={item}
        text={ISO6391.getNativeName(item).toLocaleLowerCase()}
        onClick={handleClick}
      />
    );
    return element;
  };

  return options.length > 1 && (
    <div id="translation-selector">
      <Select
        items={options}
        itemRenderer={renderItem}
        activeItem={activeElement}
        onItemSelect={handleActiveItemChange}
        filterable={false}
      >
        <Button
          text={ISO6391.getNativeName(activeElement || defaultLanguage).toLocaleLowerCase()}
          rightIcon="double-caret-vertical"
          className="translation-select-button"
          title={buttonTitleKey ? formatMessage({ id: buttonTitleKey }) : null}
        />
      </Select>
    </div>
  );
};

Translations.defaultProps = {
  onChange: null,
  buttonTitleKey: null,
};
export default Translations;
