import React, { useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { Collapse, Icon } from '@blueprintjs/core';
import * as DM from '../../chartDataManipulation';
import StatsPieChart from './StatsPieChart';
import StatsVerticalBarChart from './StatsVerticalBarChart';

interface IStatsData {
  word: string;
  wordOccurrence: number;
}

interface IWordsOccurrenceProps {
  data: IStatsData[];
}

const transformData = (data: IStatsData[]): DM.IChartData[] => data.map((item) => ({
  value: item.wordOccurrence,
  name: item.word,
}));

const WordsOccurrence = ({ data = [] }: IWordsOccurrenceProps) => {
  const [chartData, setChartData] = useState([]);
  const [open, setOpen] = useState(true);

  useEffect(() => {
    if (data.length) {
      const transformedData = transformData(data);
      setChartData(transformedData);
    } else {
      setChartData([]);
    }
  }, [data]);

  const openHandler = () => {
    if (chartData.length) {
      setOpen(!open);
    }
  };

  return (
    <div className="charts-container words-occurrence">
      <button type="button" onClick={openHandler} onKeyPress={openHandler} className={chartData.length ? 'clickable' : null}>
        <h3>
          <FormattedMessage id="charts.wordsOccurrence" />
          {!!chartData.length && <Icon icon="chevron-down" className={open ? 'open' : null} />}
        </h3>
      </button>
      { !!chartData.length && (
        <Collapse isOpen={open}>
          <StatsPieChart data={chartData} centralLabelId="charts.wordCount" parentClassName="words-occurrence" />
          <StatsVerticalBarChart data={chartData} />
        </Collapse>
      )}
      { !chartData.length && (
        <>
          <h4><FormattedMessage id="charts.nothing" /></h4>
        </>
      )}
    </div>
  );
};

export default WordsOccurrence;
