import React from 'react';

const CustomTooltip = (props: any) => {
  const { active } = props;

  if (active) {
    const { payload, label } = props;
    return (
      <div className="custom-tooltip">
        <div className="label">{`${label || payload[0]?.name} : ${payload[0]?.value}`}</div>
      </div>
    );
  }

  return null;
};

export default CustomTooltip;
