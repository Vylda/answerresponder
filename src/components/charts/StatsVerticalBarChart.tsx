import React, { useEffect, useState } from 'react';
import {
  ResponsiveContainer, BarChart, Bar, Cell, XAxis, YAxis, Tooltip, CartesianGrid,
} from 'recharts';
import { PALETTE } from '../../constants';
import * as DM from '../../chartDataManipulation';
import CustomTooltip from './CustomTooltip';

const generateCell = (data: DM.IChartData[]) => {
  const tempCells = data.map((entry, index) => (
    <Cell
      key={`cell-${entry.name}`}
      fill={PALETTE[index % PALETTE.length]}
    />
  ));
  return tempCells;
};

interface IStatsVerticalBarChartProps {
  data: DM.IChartData[];
}

const StatsVerticalBarChart = ({ data = [] }: IStatsVerticalBarChartProps) => {
  const [cells, setCells] = useState([]);

  useEffect(() => {
    if (data.length) {
      setCells(generateCell(data));
    } else {
      setCells([]);
    }
  }, [data]);

  return (
    <div className="chart stats-bar-chart" style={{ height: `${30 + (data.length * 20)}px` }}>
      <ResponsiveContainer>
        <BarChart
          data={data}
          layout="vertical"
          barCategoryGap={1}
          barSize={12}
        >
          <CartesianGrid horizontal={false} />
          <XAxis type="number" />
          <YAxis type="category" width={150} dataKey="name" interval={0} />
          <Bar dataKey="value">
            {cells}
          </Bar>
          <Tooltip cursor={false} content={<CustomTooltip />} />
        </BarChart>
      </ResponsiveContainer>
    </div>
  );
};

export default StatsVerticalBarChart;
