import React, { useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { Collapse, Icon } from '@blueprintjs/core';
import * as DM from '../../chartDataManipulation';
import StatsPieChart from './StatsPieChart';
import StatsVerticalBarChart from './StatsVerticalBarChart';

interface IStatsData {
  dayDate: string;
  wordsOccurrence: number;
}

interface IWordsPerDayProps {
  data: IStatsData[];
}

const transformData = (data: IStatsData[], locale: string): DM.IChartData[] => data.map((item) => {
  const date = new Date(item.dayDate);
  return {
    value: item.wordsOccurrence,
    name: date.toLocaleDateString(locale),
  };
});

const WordsPerDay = ({ data = [] }: IWordsPerDayProps) => {
  const [chartData, setChartData] = useState([]);
  const [open, setOpen] = useState(true);

  const { locale } = useIntl();

  useEffect(() => {
    if (data.length) {
      const transformedData = transformData(data, locale);
      setChartData(transformedData);
    } else {
      setChartData([]);
    }
  }, [data]);

  const openHandler = () => {
    if (chartData.length) {
      setOpen(!open);
    }
  };

  return (
    <div className="charts-container words-per-day">
      <button type="button" onClick={openHandler} onKeyPress={openHandler} className={chartData.length ? 'clickable' : null}>
        <h3>
          <FormattedMessage id="charts.wordsPerDay" />
          {!!chartData.length && <Icon icon="chevron-down" className={open ? 'open' : null} />}
        </h3>
      </button>
      { !!chartData.length && (
        <Collapse isOpen={open}>
          <StatsPieChart data={chartData} centralLabelId="charts.wordCount" restDataLabel="charts.otherDays" parentClassName="words-per-day" />
          <StatsVerticalBarChart data={chartData} />
        </Collapse>
      )}
      { !chartData.length && (
        <>
          <h4><FormattedMessage id="charts.nothing" /></h4>
        </>
      )}
    </div>
  );
};

export default WordsPerDay;
