import React, { useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import {
  PieChart, Pie, Cell, ResponsiveContainer, PieLabelRenderProps, Tooltip, Label,
} from 'recharts';
import { PALETTE } from '../../constants';
import * as DM from '../../chartDataManipulation';
import CustomTooltip from './CustomTooltip';

const RADIAN = Math.PI / 180;
const renderLabel = ({
  cx, cy, outerRadius, midAngle, name, value, fill,
}: PieLabelRenderProps) => {
  const x = cx as number + (outerRadius as number + 30) * Math.cos(-midAngle * RADIAN);
  const y = cy as number + (outerRadius as number + 30) * Math.sin(-midAngle * RADIAN);
  return (
    <text
      x={x}
      y={y}
      fill={fill}
      textAnchor={x > cx ? 'start' : 'end'}
      dominantBaseline="central"
    >
      {`${name} (${value})`}
    </text>
  );
};

const generateCell = (data: DM.IChartData[]) => {
  const tempCells = data.map((entry, index) => (
    <Cell
      key={`cell-${entry.name}`}
      fill={PALETTE[index % PALETTE.length]}
    />
  ));
  return tempCells;
};

const renderCentralLabel = ({ value }: PieLabelRenderProps, label: string) => (
  <g className="svg-central-label">
    <text
      x="50%"
      y="50%"
      textAnchor="middle"
    >
      {value}
    </text>
    <text
      x="50%"
      y="50%"
      dy="1.2em"
      textAnchor="middle"
    >
      <FormattedMessage id={label} />
    </text>
  </g>
);

interface IStatsPieChartProps {
  data: DM.IChartData[];
  centralLabelId?: string;
  restDataLabel?: string;
  parentClassName?: string;
}

const StatsPieChart = ({
  data = [], centralLabelId = null, restDataLabel = 'charts.otherWords', parentClassName = null,
}: IStatsPieChartProps) => {
  const [chartData, setChartData] = useState([]);
  const [cells, setCells] = useState([]);

  const { formatMessage } = useIntl();

  useEffect(() => {
    if (data.length) {
      const total = DM.dataSum(data);

      const reducedData = DM.reduceData(data);
      const hasMoreRestValues = data.length - reducedData.length > 1;
      const filteredTotal = (hasMoreRestValues)
        ? DM.dataSum(reducedData)
        : total;

      const delta = total - filteredTotal;
      if (delta > 0) {
        reducedData.push({
          name: formatMessage({ id: restDataLabel }),
          value: delta,
        });
      }

      const dataToShow = hasMoreRestValues ? reducedData : data;

      setCells(generateCell(dataToShow));
      setChartData(dataToShow);
    } else {
      setCells([]);
      setChartData([]);
    }
  }, [data]);

  return (
    <div className="chart stats-pie-chart">
      <ResponsiveContainer>
        <PieChart
          data={chartData}
        >
          <Pie
            data={chartData}
            dataKey="value"
            nameKey="name"
            paddingAngle={centralLabelId ? 3 : 0}
            innerRadius={centralLabelId ? '50%' : 0}
            label={renderLabel}
            onMouseEnter={(itemData) => {
              DM.addClassToElement(chartData, itemData.name, parentClassName);
            }}
            onMouseLeave={(itemData) => {
              DM.addClassToElement(chartData, itemData.name, parentClassName, false);
            }}
          >
            {centralLabelId && (
              <Label
                value={DM.dataSum(chartData)}
                offset={0}
                position="center"
                content={(props) => renderCentralLabel(props, centralLabelId)}
              />
            )}
            {cells}
          </Pie>
          <Tooltip content={<CustomTooltip />} />
        </PieChart>
      </ResponsiveContainer>
    </div>
  );
};

StatsPieChart.defaultProps = {
  centralLabelId: null,
  restDataLabel: 'charts.otherWords',
  parentClassName: null,
};

export default StatsPieChart;
