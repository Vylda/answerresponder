import React, { useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { Collapse, Icon } from '@blueprintjs/core';
import * as DM from '../../chartDataManipulation';
import StatsPieChart from './StatsPieChart';
import StatsVerticalBarChart from './StatsVerticalBarChart';

interface IStatsData {
  dayDate: string;
  wordOccurrence: number;
}

interface IUniqueWordsPerDayProps {
  data: IStatsData[];
}

const transformData = (data: IStatsData[], locale: string): DM.IChartData[] => data.map((item) => {
  const date = new Date(item.dayDate);
  return {
    value: item.wordOccurrence,
    name: date.toLocaleDateString(locale),
  };
});

const UniqueWordsPerDay = ({ data = [] }: IUniqueWordsPerDayProps) => {
  const [chartData, setChartData] = useState([]);
  const [open, setOpen] = useState(true);

  const { locale } = useIntl();

  useEffect(() => {
    if (data.length) {
      const transformedData = transformData(data, locale);
      setChartData(transformedData);
    } else {
      setChartData([]);
    }
  }, [data]);

  const openHandler = () => {
    if (chartData.length) {
      setOpen(!open);
    }
  };

  return (
    <div className="charts-container unique-words-per-day">
      <button type="button" onClick={openHandler} onKeyPress={openHandler} className={chartData.length ? 'clickable' : null}>
        <h3>
          <FormattedMessage id="charts.uniqueWordsPerDay" />
          {!!chartData.length && <Icon icon="chevron-down" className={open ? 'open' : null} />}
        </h3>
      </button>
      { !!chartData.length && (
        <Collapse isOpen={open}>
          <StatsPieChart data={chartData} restDataLabel="charts.otherDays" parentClassName="unique-words-per-day" />
          <StatsVerticalBarChart data={chartData} />
        </Collapse>
      )}
      { !chartData.length && (
        <>
          <h4><FormattedMessage id="charts.nothing" /></h4>
        </>
      )}
    </div>
  );
};

export default UniqueWordsPerDay;
