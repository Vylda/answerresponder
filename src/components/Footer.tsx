import React from 'react';
import { FormattedMessage } from 'react-intl';

const Footer = () => (
  <footer>
    <a
      href="https://gitlab.com/Vylda/answerresponder#answer-responder"
      target="_blank"
      rel="noreferrer"
    >
      Answer Responder&copy;
    </a>
    {' '}
    by
    {' '}
    <a
      href="https://www.geocaching.com/profile/?guid=a0055b83-b824-4d78-81bf-e63efe196850&amp;wid=2cacd74f-245a-4558-8306-072cd85a2719&amp;ds=2"
      target="_blank"
      rel="noreferrer"
    >
      Vylda
    </a>
    .
    {' '}
    2019–
    {(new Date()).getFullYear()}
    <br />
    Licence:
    {' '}
    <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.cs" target="_blank" rel="noreferrer">
      CC (BY-NC-SA)
    </a>
    <br />
    <FormattedMessage id="info" />
  </footer>
);

export default Footer;
