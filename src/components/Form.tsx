import React, { useRef, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage, useIntl } from 'react-intl';
import { FormGroup, InputGroup, Label } from '@blueprintjs/core';
import {
  setEmail, setAnswer, setNick, setErrors, setEmailError,
} from '../reduxActions';
import {
  getEmail, getAnswer, getNick, getLevel,
} from '../selectors';
import ButtonLine from './ButtonLine';

/**
 * Main answer form
 */
const Form = ({
  email,
  answer,
  nick,
  level,
  setEmailFunc,
  setAnswerFunc,
  setNickFunc,
  setErrorsFunc,
  setEmailErrorFunc,
}: {
  email: string;
  answer: string;
  nick: string;
  level: number;
  setEmailFunc: Function;
  setAnswerFunc: Function;
  setNickFunc: Function;
  setErrorsFunc: Function;
  setEmailErrorFunc: Function;
}) => {
  const emailRef = useRef(null);
  const answerRef = useRef(null);
  const nickRef = useRef(null);

  const intl = useIntl();

  const [errorEmptyAnswer, setErrorEmptyAnswer] = useState(false);
  const [errorEmptyEmail, setErrorEmptyEmail] = useState(false);
  const [errorBadEmail, setErrorBadEmail] = useState(false);
  const [errorEmptyNick, setErrorEmptyNick] = useState(false);

  /**
   * Validate form
   */
  const validate = () => {
    const answerInput = answerRef.current;
    const answerValidity = answerInput.validity;

    let nickValidity = {
      valueMissing: false,
    };
    if (level === 0 && nickRef) {
      const nickInput = nickRef.current;
      nickValidity = nickInput.validity;
    }

    let emailValidity = {
      valueMissing: false,
      typeMismatch: false,
    };

    if (level === 0 && emailRef) {
      const emailInput = emailRef.current;
      emailValidity = emailInput.validity;
    }

    setErrorEmptyAnswer(answerValidity.valueMissing);
    setErrorEmptyEmail(emailValidity.valueMissing);
    setErrorBadEmail(emailValidity.typeMismatch);
    setErrorEmptyNick(nickValidity.valueMissing);

    setEmailErrorFunc(
      emailValidity.valueMissing
      || emailValidity.typeMismatch,
    );

    setErrorsFunc(
      (
        answerValidity.valueMissing
        || emailValidity.valueMissing
        || emailValidity.typeMismatch
        || nickValidity.valueMissing
      ),
    );
  };

  useEffect(() => {
    validate();
  }, [email, answer, nick]);

  const handleChangeEmail = (e: any) => {
    setEmailFunc(e.target.value);
  };

  const handleChangeAnswer = (e: any) => {
    setAnswerFunc(e.target.value);
  };

  const handleChangeNick = (e: any) => {
    setNickFunc(e.target.value);
  };

  return (
    <form>
      <FormGroup>
        {level === 0 && (
          <>
            <Label htmlFor="input-email">
              <FormattedMessage id="form.email.label" />
            </Label>
            <InputGroup
              leftIcon="envelope"
              placeholder={intl.formatMessage({ id: 'form.email.placeholder' })}
              onChange={handleChangeEmail}
              value={email}
              type="email"
              id="input-email"
              required
              inputRef={emailRef}
            />
            {errorEmptyEmail && (
              <div className="error">
                <FormattedMessage id="error.email.empty" />
              </div>
            )}
            {errorBadEmail && (
              <div className="error">
                <FormattedMessage id="error.email.form" />
              </div>
            )}
          </>
        )}
        {level === 0 && (
          <>
            <Label htmlFor="input-nick">
              <FormattedMessage id="form.nick.label" />
            </Label>
            <InputGroup
              leftIcon="user"
              placeholder={intl.formatMessage({ id: 'form.nick.placeholder' })}
              onChange={handleChangeNick}
              value={nick}
              id="input-nick"
              required
              inputRef={nickRef}
            />
            {errorEmptyNick && (
              <div className="error">
                <FormattedMessage id="error.nick.empty" />
              </div>
            )}
          </>
        )}
        <Label htmlFor="input-answer">
          <FormattedMessage id="form.answer.label" />
        </Label>
        <InputGroup
          leftIcon="help"
          placeholder={intl.formatMessage({ id: 'form.answer.placeholder' })}
          onChange={handleChangeAnswer}
          value={answer}
          id="input-answer"
          required
          inputRef={answerRef}
        />
        {errorEmptyAnswer && (
          <div className="error">
            <FormattedMessage id="error.answer.empty" />
          </div>
        )}
        <ButtonLine />
      </FormGroup>
    </form>
  );
};

const mapStateToProps = (state: any) => ({
  email: getEmail(state),
  answer: getAnswer(state),
  nick: getNick(state),
  level: getLevel(state),
});

const mapDispatchToProps = {
  setEmailFunc: setEmail,
  setAnswerFunc: setAnswer,
  setNickFunc: setNick,
  setErrorsFunc: setErrors,
  setEmailErrorFunc: setEmailError,
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);
