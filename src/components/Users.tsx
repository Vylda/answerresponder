import React, { useState, useEffect } from 'react';
import {
  FormattedDate, FormattedMessage, FormattedTime, useIntl,
} from 'react-intl';
import { Button, HTMLTable } from '@blueprintjs/core';

interface IUser {
  email: string;
  nick: string;
  current_level: number;

  current_question_id: number;

  timestamp: string;
}

export interface ICallbackProps {
  email: string;
  nick: string;
}

interface IUserProps {
  users: IUser[];
  onDelete: (userData: ICallbackProps) => void;
}

const Users = ({ users, onDelete }: IUserProps) => {
  const [rows, setRows] = useState([]);

  const { formatMessage } = useIntl();

  const clickHandler = (email: string, nick: string) => {
    onDelete({ email, nick });
  };

  useEffect(() => {
    const renderedRows = users.map((user: IUser) => {
      const cells = [];
      const timeArray = user.timestamp.split(/[- :]/).map((num) => parseInt(num, 10));
      const date = new Date(Date.UTC(
        timeArray[0], timeArray[1] - 1, timeArray[2], timeArray[3], timeArray[4], timeArray[5],
      ));
      cells.push(
        <td key={`${user.email}-nick`}><b>{user.nick}</b></td>,
        <td key={`${user.email}-level`} className="right">{user.current_level}</td>,
        <td key={`${user.email}-current`} className="right">{user.current_question_id}</td>,
        <td key={`${user.email}-date`} className="right">
          <FormattedDate value={date} />
          {' '}
          <FormattedTime value={date} />
        </td>,
        <td key={`${user.email}-delete`} className="center">
          <Button
            onClick={() => clickHandler(user.email, user.nick)}
          >
            <FormattedMessage id="users.delete" />
          </Button>
        </td>,
      );
      return <tr key={user.email}>{cells}</tr>;
    });
    setRows(renderedRows);
  }, [users]);

  return (
    <div className="users">
      <h2><FormattedMessage id="users.title" /></h2>
      {!!rows.length
        && (
          <div className="table-container">
            <HTMLTable bordered condensed striped interactive>
              <thead>
                <tr>
                  <th
                    title={formatMessage({ id: 'users.nick' })}
                  >
                    <FormattedMessage id="users.nick" />
                  </th>
                  <th
                    title={formatMessage({ id: 'users.currentLevel' })}
                  >
                    <FormattedMessage id="users.currentLevel" />
                  </th>
                  <th
                    title={formatMessage({ id: 'users.lastAnswerNumber' })}
                  >
                    <FormattedMessage id="users.lastAnswerNumber" />
                  </th>
                  <th
                    title={formatMessage({ id: 'users.lastAnswerDate' })}
                  >
                    <FormattedMessage id="users.lastAnswerDate" />
                  </th>
                  <th
                    title={formatMessage({ id: 'users.delete' })}
                  >
                    <FormattedMessage id="users.delete" />
                  </th>
                </tr>
              </thead>
              <tbody>
                {rows}
              </tbody>
            </HTMLTable>
          </div>
        )}
      {!rows.length
        && (
          <p><FormattedMessage id="users.nothing" /></p>
        )}
    </div>
  );
};
export default Users;
