import React, { useState, useEffect } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { Button } from '@blueprintjs/core';
import { DateRangeInput, IDateRangeShortcut } from '@blueprintjs/datetime';
import LocaleUtils from '../LocaleUtils';
import WordsOccurrence from './charts/WordsOccurrence';
import WordsPerDay from './charts/WordsPerDay';
import UniqueWordsPerDay from './charts/UniqueWordsPerDay';

interface IChartsProps {
  data: any;
  datums: {
    endDate: string;
    startDate: string;
  }
  onRefresh: Function;
}

const Charts = ({ data, datums, onRefresh }: IChartsProps) => {
  const [start, setStart] = useState(new Date());
  const [end, setEnd] = useState(new Date());
  const [correctRange, setCorrectRange] = useState(true);
  const [titleDatums, setTitleDatums] = useState({ start: '', end: '' });

  const { formatMessage, locale } = useIntl();

  const shortCuts: IDateRangeShortcut[] = [
    {
      dateRange: [new Date(), new Date()],
      label: 'dnes',
    },
    {
      dateRange: [
        new Date(new Date().setDate(new Date().getDate() - 1)),
        new Date(new Date().setDate(new Date().getDate() - 1)),
      ],
      label: 'včera',
    },
    {
      dateRange: [
        new Date(new Date().setDate(new Date().getDate() - 7)),
        new Date(),
      ],
      label: 'poslední týden',
    },
    {
      dateRange: [
        new Date(new Date().setDate(new Date().getDate() - 14)),
        new Date(),
      ],
      label: 'poslední dva týdny',
    },
    {
      dateRange: [
        new Date(new Date().setMonth(new Date().getMonth() - 1)),
        new Date(),
      ],
      label: 'poslední měsíc',
    },
    {
      dateRange: [
        new Date(new Date().setMonth(new Date().getMonth() - 3)),
        new Date(),
      ],
      label: 'poslední tři měsíce',
    },
    {
      dateRange: [
        new Date(new Date().setMonth(new Date().getMonth() - 6)),
        new Date(),
      ],
      label: 'poslední půlrok',
    },
  ];

  useEffect(() => {
    if (datums) {
      const { endDate, startDate } = datums;
      const startTemp = new Date(startDate);
      startTemp.setHours(0);
      const endTemp = new Date(endDate);
      endTemp.setHours(23);
      endTemp.setMinutes(59);
      endTemp.setSeconds(59);
      setStart(startTemp);
      setEnd(endTemp);
      setTitleDatums({
        start: startTemp.toLocaleDateString(locale),
        end: endTemp.toLocaleDateString(locale),
      });
    }
  }, [datums]);

  const handleRangeChange = (range: [Date, Date]) => {
    try {
      setCorrectRange(true);
      if (range[0]) {
        if (range[0].toString() !== 'Invalid Date') {
          setStart(range[0]);
        } else {
          throw new Error('Invalid start Date');
        }
      } else {
        throw new Error('start Date not set');
      }
      if (range[1]) {
        if (range[1].toString() !== 'Invalid Date') {
          setEnd(range[1]);
        } else {
          throw new Error('Invalid end Date');
        }
      } else {
        throw new Error('end Date not set');
      }
    } catch (err) {
      console.error(err.message);
      setCorrectRange(false);
    }
  };

  const refreshHandler = () => {
    if (onRefresh) {
      const sYear = start.getFullYear();
      const sMonth = (start.getMonth() + 1).toString(10).padStart(2, '0');
      const sDay = start.getDate().toString(10).padStart(2, '0');
      const newStart = `${sYear}-${sMonth}-${sDay}`;

      const eYear = end.getFullYear();
      const eMonth = (end.getMonth() + 1).toString(10).padStart(2, '0');
      const eDay = end.getDate().toString(10).padStart(2, '0');
      const newEnd = `${eYear}-${eMonth}-${eDay}`;

      onRefresh({ startDate: newStart, endDate: newEnd });
    }
  };

  const dateFormatter = (date: Date): string => {
    const formattedDate = date.toLocaleDateString(locale);
    return formattedDate;
  };

  const dateParser = (date: string, localeString: string): Date => {
    const parsedDate = LocaleUtils.parseDate(date, '', localeString);
    return parsedDate;
  };

  return (
    <div className="charts">
      <h2>
        <FormattedMessage
          id="charts.title"
          values={{
            from: titleDatums.start,
            to: titleDatums.end,
          }}
        />
      </h2>
      <div className="date-line">
        <DateRangeInput
          className="date-picker"
          onChange={handleRangeChange}
          formatDate={dateFormatter}
          parseDate={dateParser}
          maxDate={new Date()}
          locale={locale}
          localeUtils={LocaleUtils}
          highlightCurrentDay
          allowSingleDayRange
          value={[start, end]}
          outOfRangeMessage={formatMessage({ id: 'charts.error.outOfRangeMessage' })}
          overlappingDatesMessage={formatMessage({ id: 'charts.error.overlappingDatesMessage' })}
          invalidDateMessage={formatMessage({ id: 'charts.error.invalidDateMessage' })}
          shortcuts={shortCuts}
          onError={() => setCorrectRange(false)}
        />
        <Button
          onClick={refreshHandler}
          disabled={!correctRange}
        >
          <FormattedMessage id="charts.refresh" />
        </Button>
      </div>
      <div className="main-charts-container">
        <WordsOccurrence data={data.wordsStat} />
        <WordsPerDay data={data.wordsPerDay} />
        <UniqueWordsPerDay data={data.uniqueWordsPerDay} />
      </div>
    </div>
  );
};
export default Charts;
