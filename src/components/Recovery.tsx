import React, { useRef, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage, useIntl } from 'react-intl';
import {
  FormGroup, InputGroup, Label, Overlay, Classes,
  Card, Elevation, Tooltip, Button, Intent,
} from '@blueprintjs/core';
import { setRecovery } from '../reduxActions';
import { getRecoveryMode, getRecoveryError } from '../selectors';
import { resetGame } from '../helpers';
import { recoveryModes } from '../constants';

/**
 * Recovery modal window
 */
const Recovery = ({
  recoveryMode,
  recoveryError,
  setRecoveryModeFunc,
}: {
  recoveryMode: boolean;
  recoveryError: string;
  setRecoveryModeFunc: Function;
}) => {
  const passwordRef = useRef(null);
  const [errorEmptyPassword, setErrorEmptyPassword] = useState(true);
  const [password, setPassword] = useState('');
  const [isOpen, setIsOpen] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  const intl = useIntl();

  /**
   * Validate form
   */
  const validate = () => {
    const passwordInput = passwordRef.current;
    if (passwordInput) {
      const passwordValidity = passwordInput.validity;
      setErrorEmptyPassword(passwordValidity.valueMissing);
    }
  };

  useEffect(() => {
    validate();
  }, [password]);

  useEffect(() => {
    setIsOpen(recoveryMode);
  }, [recoveryMode]);

  const handleChangePassword = (e: any) => {
    setPassword(e.target.value);
  };

  const handleClose = () => {
    setRecoveryModeFunc({
      recoveryMode: false,
      recoveryError: '',
    });
  };

  const handleReset = () => {
    resetGame(password, recoveryModes.DELETE);
  };

  const handleRecovery = () => {
    resetGame(password, recoveryModes.RECOVER);
  };

  const handleLockClick = () => {
    setShowPassword(!showPassword);
  };

  /**
   * Create paragraphs from array of text
   */
  const makeInfos = ():any => {
    const infos = intl.formatMessage({
      id: 'recovery.info',
    }).split(/\n+/);
    const paragraphs = infos.map((text, index) => {
      const key = `info-text-${index}`;
      return (
        <p key={key}>
          {text}
          <br />
        </p>
      );
    });
    return paragraphs;
  };

  /**
   * Lock button for password field
   */
  const lockButton = (
    <Tooltip content={intl.formatMessage({
      id: 'recovery.showHidePassword',
    },
    {
      showHide: intl.formatMessage({
        id: showPassword ? 'recovery.hide' : 'recovery.show',
      }),
    })}
    >
      <Button
        icon={showPassword ? 'unlock' : 'lock'}
        intent={Intent.WARNING}
        minimal
        onClick={handleLockClick}
      />
    </Tooltip>
  );

  return (
    <>
      <Overlay
        isOpen={isOpen}
        className={Classes.OVERLAY_SCROLL_CONTAINER}
        autoFocus
        enforceFocus
        canOutsideClickClose={false}
        canEscapeKeyClose
      >
        <Card elevation={Elevation.THREE} className="recovery-content">
          <h2 className="bp3-heading">
            <FormattedMessage id="recovery.title" />
          </h2>
          <div className="bp3-ui-text recovery-info">
            {makeInfos()}
          </div>
          {recoveryError && <div className="recovery-error">{recoveryError}</div>}
          <FormGroup>
            <Label htmlFor="input-password">
              <FormattedMessage id="recovery.label" />
            </Label>
            <InputGroup
              leftIcon="key"
              placeholder={intl.formatMessage({ id: 'recovery.placeholder' })}
              onChange={handleChangePassword}
              value={password}
              id="input-password"
              required
              inputRef={passwordRef}
              rightElement={lockButton}
              type={showPassword ? 'text' : 'password'}
            />
            {errorEmptyPassword && (
            <div className="error">
              <FormattedMessage id="recovery.errorEmptyPassword" />
            </div>
            )}
          </FormGroup>
          <div className="button-line">
            <Button
              onClick={handleClose}
            >
              <FormattedMessage id="recovery.close" />
            </Button>
            <Button
              disabled={errorEmptyPassword}
              onClick={handleReset}
              intent={Intent.WARNING}
            >
              <FormattedMessage id="recovery.delete" />
            </Button>
            <Button
              disabled={errorEmptyPassword}
              onClick={handleRecovery}
              intent={Intent.SUCCESS}
            >
              <FormattedMessage id="recovery.recover" />
            </Button>
          </div>
        </Card>
      </Overlay>
    </>
  );
};

const mapStateToProps = (state: any) => ({
  recoveryMode: getRecoveryMode(state),
  recoveryError: getRecoveryError(state),
});

const mapDispatchToProps = {
  setRecoveryModeFunc: setRecovery,
};

export default connect(mapStateToProps, mapDispatchToProps)(Recovery);
