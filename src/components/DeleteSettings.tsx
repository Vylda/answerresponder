import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Button } from '@blueprintjs/core';

type IDeleteSettingsProps = {
  onClick?: Function;
};

const DeleteSettings = ({ onClick }: IDeleteSettingsProps) => {
  const onClickHandler = () => {
    if (onClick) {
      onClick();
    }
  };

  return (
    <Button
      onClick={onClickHandler}
      icon="settings"
    >
      <FormattedMessage id="deleteSettings" />
    </Button>
  );
};

DeleteSettings.defaultProps = {
  onClick: null,
};
export default DeleteSettings;
