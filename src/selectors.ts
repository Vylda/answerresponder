import { createSelector } from 'reselect';

const formSelector = (state: any) => state.form;
const tasksSelector = (state: any) => state.task;
const errorsSelector = (state: any) => state.errors;
const errorEmailSelector = (state: any) => state.emailError;
const beErrorMessageSelector = (state: any) => state.beErrorMessage;
const isBadAnswerSelector = (state: any) => state.isBadAnswer;
const loadingSelector = (state: any) => state.loading;
const recoverySelector = (state: any) => state.recovery;

export const getEmail = createSelector(formSelector, (form) => form.email);

export const getAnswer = createSelector(formSelector, (form) => form.answer);

export const getNick = createSelector(formSelector, (form) => form.nick);

export const getTask = createSelector(tasksSelector, (task) => task);

export const getLevel = createSelector(tasksSelector, (task) => task.level);

export const getCompleted = createSelector(tasksSelector, (task) => task.completed);

export const getErrors = createSelector(errorsSelector, (errors) => errors);

export const getEmailError = createSelector(errorEmailSelector, (emailError) => emailError);

export const getBeErrorMessage = createSelector(
  beErrorMessageSelector,
  (beErrorMessage) => beErrorMessage,
);

export const getIsBadAnswer = createSelector(
  isBadAnswerSelector,
  (isBadAnswer) => isBadAnswer,
);

export const getLoading = createSelector(loadingSelector, (loading) => loading);

export const getRecoveryMode = createSelector(
  recoverySelector,
  (recovery) => recovery.recoveryMode,
);

export const getRecoveryError = createSelector(
  recoverySelector,
  (recovery) => recovery.recoveryError,
);

export const getSuccessMessage = createSelector(
  recoverySelector,
  (recovery) => recovery.successMessage,
);
