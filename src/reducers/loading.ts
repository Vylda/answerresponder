import { SET_LOADING } from '../constants';

const defaultState = true;

const loading = (state: boolean = defaultState, action: any) => {
  switch (action.type) {
    case SET_LOADING:
      return action.loading;
    default:
      return state;
  }
};

export default loading;
