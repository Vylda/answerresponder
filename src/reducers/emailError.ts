import { SET_EMAIL_ERRORS } from '../constants';

const defaultState = false;

const emailError = (state: any = defaultState, action: any) => {
  switch (action.type) {
    case SET_EMAIL_ERRORS:
      return action.error;
    default:
      return state;
  }
};

export default emailError;
