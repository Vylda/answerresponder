import { CHANGE_TASK } from '../constants';
import { intl } from '../langResolver';

const zeroTask: {
  hints: string[];
  title: string;
} = {
  hints: intl.formatMessage({ id: 'task.first.hints' }).split(/\n/),
  title: intl.formatMessage({ id: 'task.first.title' }),
};

export const defaultState = {
  level: 0,
  completed: false,
  hints: zeroTask.hints,
  title: zeroTask.title,
};

const tasks = (state: any = defaultState, action: any) => {
  const {
    level, completed, hints, title,
  } = action;
  switch (action.type) {
    case CHANGE_TASK:
      return {
        ...state,
        level,
        completed,
        hints,
        title,
      };
    default:
      return state;
  }
};

export default tasks;
