import { SET_BE_ERROR_MESSAGE } from '../constants';

const defaultState = '';

const beErrorMessage = (state: any = defaultState, action: any) => {
  switch (action.type) {
    case SET_BE_ERROR_MESSAGE:
      return action.errorMessage;
    default:
      return state;
  }
};

export default beErrorMessage;
