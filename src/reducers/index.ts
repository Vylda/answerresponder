import { combineReducers } from 'redux';
import form from './form';
import task from './task';
import errors from './errors';
import emailError from './emailError';
import beErrorMessage from './beErrorMessage';
import loading from './loading';
import recovery from './recoveryMode';
import isBadAnswer from './isBadAnswer';

export default combineReducers({
  form,
  task,
  errors,
  beErrorMessage,
  emailError,
  loading,
  recovery,
  isBadAnswer,
});
