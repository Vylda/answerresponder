import { CHANGE_EMAIL, CHANGE_ANSWER, CHANGE_NICK } from '../constants';

const defaultState = {
  email: '',
  answer: '',
  nick: '',
};

const form = (state: any = defaultState, action: any) => {
  switch (action.type) {
    case CHANGE_ANSWER:
      return {
        ...state,
        answer: action.answer,
      };
    case CHANGE_EMAIL:
      return {
        ...state,
        email: action.email,
      };
    case CHANGE_NICK:
      return {
        ...state,
        nick: action.nick,
      };
    default:
      return state;
  }
};

export default form;
