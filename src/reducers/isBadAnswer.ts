import { SET_IS_BAD_ANSWER } from '../constants';

const defaultState = false;

const isBadAnswer = (state: any = defaultState, action: any) => {
  switch (action.type) {
    case SET_IS_BAD_ANSWER:
      return action.isBadAnswer;
    default:
      return state;
  }
};

export default isBadAnswer;
