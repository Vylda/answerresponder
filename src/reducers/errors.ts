import { SET_ERRORS } from '../constants';

const defaultState = false;

const errors = (state: any = defaultState, action: any) => {
  switch (action.type) {
    case SET_ERRORS:
      return action.errors;
    default:
      return state;
  }
};

export default errors;
