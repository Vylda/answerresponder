import { SET_RECOVERY_MODE } from '../constants';

const defaultState = {
  recoveryMode: false,
  recoveryError: '',
  successMessage: '',
};

export interface IRecovery {
  recoveryMode: boolean,
  recoveryError: string,
  successMessage: string,
}

const recoveryMode = (state: IRecovery = defaultState, action: any) => {
  switch (action.type) {
    case SET_RECOVERY_MODE:
      return action.recovery;
    default:
      return state;
  }
};

export default recoveryMode;
