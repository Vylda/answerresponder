import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, connect } from 'react-redux';
import { IntlProvider, FormattedMessage } from 'react-intl';
import {
  Spinner, Toaster, Toast, Intent, Position, Card, Elevation,
} from '@blueprintjs/core';
import {
  langCode, messages, intl, getTranslations,
} from './langResolver';
import Form from './components/Form';
import Footer from './components/Footer';
import Question from './components/Question';
import Recovery from './components/Recovery';
import DarkMode, { MODES } from './components/DarkMode';
import Translations from './components/Translations';
import DeleteSettings from './components/DeleteSettings';
import { getStoredEmail, start } from './helpers';
import { setEmail, changeTask } from './reduxActions';
import {
  getLoading, getCompleted, getSuccessMessage, getLevel,
} from './selectors';
import { defaultState as defTask } from './reducers/task';

interface IConfig {
  INSTANCE_NAME: string;
}

/**
 * Global variables
 */
declare global {
  interface Window {
    gameConfig: IConfig;
  }
}

interface IAppProps {
  loading: boolean;
  completed: boolean;
  successMessage: string;
  level: number;
  changeTaskAction: Function;
}

const App = ({
  loading, completed, successMessage, level, changeTaskAction,
}: IAppProps) => {
  const { formatMessage } = intl;
  const dispatch = useDispatch();
  const [toasts] = useState([]);
  const [localeState, setLocaleState] = useState(langCode);
  const [messagesState, setMessagesState] = useState(messages);
  const [defaultMode, setdefaultMode] = useState(MODES.NONE);
  const toasterRef = useRef(null);

  const setZeroTask = (currentLevel: number, newMessages: any) => {
    if (currentLevel === 0) {
      const newTask = {
        ...defTask,
        title: newMessages['task.first.title'],
        hints: newMessages['task.first.hints'].split(/\n/),
      };
      changeTaskAction(newTask);
    }
  };

  const languageChangeHandler = (newLanguage: string) => {
    localStorage.setItem(`${window.gameConfig.INSTANCE_NAME}_defaultLanguage`, newLanguage);
    const newTranslation = getTranslations();

    setMessagesState(newTranslation.messages);
    setLocaleState(newTranslation.langCode);
    setZeroTask(level, newTranslation.messages);
  };

  const modeChangeHandler = (mode: MODES) => {
    localStorage.setItem(`${window.gameConfig.INSTANCE_NAME}_acDarkMode`, mode.toString());
  };

  const deleteSettingsHandler = () => {
    localStorage.removeItem(`${window.gameConfig.INSTANCE_NAME}_acDarkMode`);
    localStorage.removeItem(`${window.gameConfig.INSTANCE_NAME}_defaultLanguage`);
    const newTranslation = getTranslations();
    setLocaleState(newTranslation.langCode);
    setMessagesState(newTranslation.messages);
    setZeroTask(level, newTranslation.messages);
    setdefaultMode(MODES.NONE);
  };

  useEffect(() => {
    const modeNumber: number = parseInt(
      localStorage.getItem(`${window.gameConfig.INSTANCE_NAME}_acDarkMode`), 10,
    ) || 0;
    const mode = modeNumber as MODES;
    setdefaultMode(mode);

    document.title = intl.formatMessage({ id: 'name' });
    const email = getStoredEmail();
    dispatch(setEmail(email));
    start();
  }, []);

  useEffect(() => {
    if (successMessage) {
      toasterRef.current.show({
        message: successMessage,
        intent: Intent.SUCCESS,
        timeout: 5000,
      });
    }
  }, [successMessage]);

  useEffect(() => {
    if (completed) {
      toasterRef.current.show({
        message: formatMessage({ id: 'personalDataDeleted' }),
        intent: Intent.SUCCESS,
        timeout: 5000,
      });
    }
  }, [completed]);

  useEffect(() => {
    const modeNumber: number = parseInt(
      localStorage.getItem(`${window.gameConfig.INSTANCE_NAME}_acDarkMode`), 10,
    ) || 0;
    const mode = modeNumber as MODES;
    setdefaultMode(mode);
  }, [localeState]);

  return (
    <IntlProvider key={localeState} locale={localeState} messages={messagesState}>
      {loading
        ? (
          <div className="loader">
            <Spinner intent="primary" size={Spinner.SIZE_LARGE} />
          </div>
        )
        : (
          <>
            <Card elevation={3}>
              <h1>
                <FormattedMessage id="name" />
              </h1>
              <Question />
              {!completed && <Form />}
              <Footer />
            </Card>
            <Card elevation={Elevation.THREE} className="app-settings">
              <DarkMode switchTitleKey="darkMode" defaultMode={defaultMode} onChange={modeChangeHandler} />
              <Translations
                defaultLanguage={localeState}
                onChange={languageChangeHandler}
                buttonTitleKey="setTranslation"
              />
              <DeleteSettings onClick={deleteSettingsHandler} />
            </Card>
            <Recovery />
          </>
        )}

      <Toaster
        position={Position.TOP}
        ref={toasterRef}
      >
        {toasts.map((toast) => (
          <Toast intent={toast.success} message={toast.message} />
        ))}
      </Toaster>
    </IntlProvider>
  );
};

const mapStateToProps = (state: any) => ({
  loading: getLoading(state),
  completed: getCompleted(state),
  successMessage: getSuccessMessage(state),
  level: getLevel(state),
});

const mapDispatchToProps = {
  changeTaskAction: changeTask,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
