import { minPieValueDeg } from './constants';

export interface IChartData {
  name: string;
  value: number;
}

/**
 * Sumarize data values
 * @param data Datat to sum
 */
export const dataSum = (data: IChartData[]): number => data.reduce((
  tot: number,
  item: IChartData,
) => {
  let temp = tot;
  temp += item.value;
  return temp;
}, 0);

/**
 * return mini value from data
 * @param data
 */
export const minValue = (data: IChartData[]): number => data.reduce((
  min: number,
  item: IChartData,
) => {
  const wo: number = item.value;
  return min < wo ? min : wo;
}, data[0].value);

/**
 * Reduce data for pie chart in dependecy to mimimal element angle
 * @param data
 */
export const reduceData = (data: IChartData[]): IChartData[] => {
  let tempData = [...data];
  const minimalWordOccurence = minValue(tempData);
  const total = dataSum(tempData);
  const minValuePerDeg = minimalWordOccurence * (360 / total);
  if (minValuePerDeg < minPieValueDeg) {
    const filteredData = tempData.filter((item) => item.value > minimalWordOccurence);
    if (filteredData.length) {
      tempData = reduceData(filteredData);
    }
  }
  return tempData;
};

/**
 * Add class name over to element with current name
 * @param chartData
 * @param name
 * @param className
 * @param add
 */
export const addClassToElement = (
  chartData: IChartData[],
  name: string,
  className: string,
  add: boolean = true,
): void => {
  if (!className) {
    return;
  }
  const getIndex = (chData: any, iName: string) => chData.reduce((
    acc: number, item: any, index: number,
  ) => {
    if (item.name === iName) {
      return index;
    }
    return acc;
  }, -1);
  const indexElement = getIndex(chartData, name);
  const elements = document.querySelectorAll(`.${className} g.recharts-pie-sector`);
  const element = elements[indexElement];
  if (element) {
    element.classList[add ? 'add' : 'remove']('over');
  }
};
