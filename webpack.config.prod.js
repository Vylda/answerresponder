const path = require('path');
const { merge } = require('webpack-merge');
const TerserJSPlugin = require('terser-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const common = require('./webpack.config.common.js');
const webpack = require('webpack');

module.exports = merge(common, {
  mode: 'production',
  performance: {
    maxEntrypointSize: 2700000,
    maxAssetSize: 2700000,
  },
  optimization: {
    minimizer: [
      new TerserJSPlugin({
      extractComments: true,
      exclude: [
        'config.js',
      ]
      }),
      new CssMinimizerPlugin(),
    ],
    splitChunks: {
      chunks: 'all',
    },
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: 'Answer responder',
      filename: 'index.html',
      template: './template/index.html',
      chunks: ['index'], minify: {
        collapseWhitespace: true,
        removeComments: true,
        quoteCharacter: `"`,
        removeRedundantAttributes: true,
        useShortDoctype: true,
      },
    }),
    new HtmlWebpackPlugin({
      title: 'Answer responder – password generator',
      filename: 'password.html',
      template: './template/index.html',
      chunks: ['password'], minify: {
        collapseWhitespace: true,
        removeComments: true,
        quoteCharacter: `"`,
        removeRedundantAttributes: true,
        useShortDoctype: true,
      },
    }),
    new HtmlWebpackPlugin({
      title: 'Answer responder – statistics',
      filename: 'stats.html',
      template: './template/index.html',
      chunks: ['stats'], minify: {
        collapseWhitespace: true,
        removeComments: true,
        quoteCharacter: `"`,
        removeRedundantAttributes: true,
        useShortDoctype: true,
      },
    }),
    new webpack.DefinePlugin({
      PRODUCTION: JSON.stringify(true),
    }),
  ],
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        options: {
          configFile: './.eslintrc.prod.js',
        },
      },
      {
        test: /\.(ts|tsx)$/,
        loader: 'awesome-typescript-loader',
        options: {
          configFileName: 'tsconfig.prod.json',
        },
        exclude: /node_modules/,
      },
    ],
  },
});
